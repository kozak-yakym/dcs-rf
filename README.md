# Radio Frequency Data Collecting System Project

Welcome to the repository presenting the Radio Frequency Data Collecting System (RF DCS). This project focuses on the design and implementation of a system for monitoring and collecting data from various sensors, with a primary emphasis on temperature measurement, using RF communication.
I've built this project from scratch back in 2015 to investigate how a boiler works on firewood. I had sensors for supply, return, firewood door where firewood was loaded, and a sensor in one of the heated rooms. I also installed one sensor outside, but it refused to work in temperatures under 0 degrees Celsius. It was probably a battery issue. So I decided to take the outdoor temperature from the weather server API for my city.
At that time, I already had some experience with microcontrollers and a dozen completed simple projects, but I had just started learning Python and didn't know how to visualize it. So I just saved the data in several files. It was only in 2020 that I decided to apply my new Python knowledge to visual data analysis. You can check my visualization tool in my [GitHub repo](https://github.com/kozak-yakym/visualisation_tool).

## System Overview

At the heart of this system is a PC application written in Python ([RF_DCS_py](Code/RF_DCS_py/main.py)), which interfaces with a virtual COM port over USB. This connection is established to communicate with a radio module hub constructed around an ATmega8 microcontroller. This module is further equipped with an FT232 USB-to-serial converter and an RFM70 radio module operating at 433MHz. Development of this component was carried out in the CVAvr environment.

### Sensor Nodes

The sensor nodes, pivotal to data collection, are built on ATTiny13 microcontrollers. These nodes are also furnished with RFM70 radio modules operating at 433MHz and primarily utilize DS18b20 temperature sensors. A standout feature within the system is a specialized sensor node designed for high-temperature environments, such as a wood-fired boiler furnace. This node employs a thermocouple, amplified by an analog signal amplifier, with its output read by an ATTiny13 microcontroller's ADC. Achieving accurate measurements necessitated calibration against a reference thermometer capable of reading up to 2000 degrees Celsius.

The sensor node codebase is crafted in C, utilizing AtmelStudio, and can be found within the [Tiny_RF_Sensor](Code/Tiny_RF_Sensor/) directory.

## Repository Structure

- **RF_DCS**: Hosts the central data collection unit's code, including protocols for the reception and decoding of RF signals.
- **Tiny_RF_Sensor**: Contains sensor node code, detailing RF signal transmission via the `sendRC()` function and integration with temperature sensors.
- **RF_DCS_py**: Contains Python code for reading data from the hub and the weather service.

## Development Journey

The project's lifecycle spanned multiple phases, from conceptualization to deployment and beyond:

1. Initial prototyping and sensor evaluation.
2. Development of the RF communication protocol.
3. Debugging under real-world conditions, including power management and ADC considerations.
4. Integration and testing of the thermocouple sensor.
5. Prototyping for field deployment.
6. Production of the final devices: 1 server, 1 receiver, 3 DS18b20 sensor nodes, and 1 thermocouple sensor node.
7. Launch of the system and analysis of collected data.
8. Finalization of device PCB designs and assembly.
9. Specification of system requirements and software development.
10. Comprehensive testing and debugging phase.
11. Transition to mass production. (Future plans)

## Future Directions

Subsequent projects leveraging this system's architecture planned to include a fuel burnout notification device and a solid fuel boiler control system, each undergoing its unique development process.

For now, I don't plan to maintain this code and leave it as it is, as I wrote it in 2015.