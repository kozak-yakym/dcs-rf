/*
 * Tiny_RF_Sensor.c
 *
 * Created: 29.09.2015 8:30:05
 *  Author: kozak-yakym
 */ 

#define timer			1//15 // ����� � �������� (���������� �� 8) ����� � �������� ������ � �������.
#define periodusec		400 // mcs
#define DS_BIT			4 // pin 3
#define RC_BIT			3 // pin 2
#define ADC_IN			1 // # of ADC input
#define keyT			11000 //
#define IDT				12000 //16bit
#define SENDTCOM		0x05 //8bit
#define SENDVCOM		0x0B //8bit

#include <avr/io.h>
#include <avr/wdt.h>      // ����� ������������ ������ � ���������
#include <avr/sleep.h>      // ����� ������� ������ ���
#include <avr/interrupt.h>                 // ������ � ������������
#include <util/delay.h>     /* for _delay_us() */

// Bandgap Voltage Reference: Off
#define ADC_VREF_TYPE ((0<<REFS0) | (0<<ADLAR))


// Watchdog timeout interrupt service routine
// interrupt [WDT] void wdt_timeout_isr(void)
// {
// 	// Place your code here
// 
// }


// ���������� ���������� //������-�� ���� �� �������
ISR (WDT_vect)
{
	WDTCR |=_BV(WDTIE);   // ��������� ���������� �� ��������, ����� ����� �����!
	wdt_reset();         // �����
}


// Read the AD conversion result
unsigned int read_adc(unsigned char adc_input)
{
	ADMUX=adc_input | ADC_VREF_TYPE;
	// Delay needed for the stabilization of the ADC input voltage
	_delay_us(10);
	// Start the AD conversion
	ADCSRA|=(1<<ADSC);
	// Wait for the AD conversion to complete
	while ((ADCSRA & (1<<ADIF))==0);
	ADCSRA|=(1<<ADIF);
	return ADCW;
}



//CRC-8 - based on the CRC8 formulas by Dallas/Maxim
//code released under the therms of the GNU GPL 3.0 license
unsigned char CRC8(const unsigned char *data, unsigned char len) 
{
	unsigned char crc = 0x00;
	while (len--) 
	{
		unsigned char extract = *data++;
		for (unsigned char tempI = 8; tempI; tempI--) 
		{
			unsigned char sum = (crc ^ extract) & 0x01;
			crc >>= 1;
			if (sum) 
			{
				crc ^= 0x8C;
			}
			extract >>= 1;
		}
	}
	return crc;
}

void RC_routine(uint8_t *data_arr, uint8_t d_lenght)
{
	for(uint8_t j=0; j<d_lenght; j++)
	{
		for (uint8_t i=0; i<8; i++)
		{
			switch (data_arr[j] & (uint8_t)(1<<i))
			{

				case 0:
				{
					PORTB |= _BV(RC_BIT);
					_delay_us(periodusec);
					PORTB &= ~_BV(RC_BIT);
					_delay_us(periodusec*3);
				}
				break;

				default:  //not 0, (1)
				{
					PORTB |= _BV(RC_BIT);
					_delay_us(periodusec*3);
					PORTB &= ~_BV(RC_BIT);
					_delay_us(periodusec);					
				}								
				break;
			}
		}	
	}
		//��������� ��� �������������
		PORTB |= _BV(RC_BIT);
		_delay_us(periodusec);
		PORTB &= ~_BV(RC_BIT);
		_delay_us(periodusec*31);
}

void sendRC(uint8_t command, uint16_t data)  // �������� ������ �� ����������� RCswitch. �������� ��������
{
	//����������� �� ����� ����� �����������
	DDRB |= _BV(RC_BIT);
//	data |= 3L << 20; // ?
	unsigned short repeats = 8; //1 << (((unsigned long)data >> 20) & 7);

	data = data & 0xffff;
	
    uint8_t dataBase[6] = {0};
//    uint8_t dataBaseREV[6] = 0;
	
	//���������� ID
	dataBase[0]=(uint8_t)(IDT>>8);
	dataBase[1]=(uint8_t)(IDT);
	//���������� ��� ��������
	dataBase[2]=(uint8_t)(command);
	//���������� data
	dataBase[3]=(uint8_t)(data>>8);
	dataBase[4]=(uint8_t)(data);
	//���������� CRC8
	dataBase[5]=CRC8(dataBase, 5);


	for (unsigned short int j=0;j<repeats;j++) 
	{
		uint8_t i;
		
		//���������� �������
		RC_routine(dataBase, 6);
	}
}

// OneWire �������:

void OneWireReset()
{

	PORTB &= ~_BV(DS_BIT);
	DDRB |= _BV(DS_BIT);
	_delay_us(500);
	DDRB &= ~_BV(DS_BIT);
	_delay_us(500);
}

void OneWireOutByte(uint8_t d)
{
	uint8_t n;

	for(n=8; n!=0; n--)
	{
		if ((d & 0x01) == 1)
		{

			PORTB &= ~_BV(DS_BIT);
			DDRB |= _BV(DS_BIT);
			_delay_us(5);
			DDRB &= ~_BV(DS_BIT);
			_delay_us(60);
		}
		else
		{
			PORTB &= ~_BV(DS_BIT);
			DDRB |= _BV(DS_BIT);
			_delay_us(60);
			DDRB &= ~_BV(DS_BIT);
		}
		d=d>>1;
	}
}


uint8_t OneWireInByte()
{
	uint8_t d, n,b;

	for (n=0; n<8; n++)
	{
		PORTB &= ~_BV(DS_BIT);
		DDRB |= _BV(DS_BIT);
		_delay_us(5);
		DDRB &= ~_BV(DS_BIT);
		_delay_us(5);
		b = ((PINB & _BV(DS_BIT)) != 0);
		_delay_us(50);
		d = (d >> 1) | (b<<7);
	}
	return(d);
}

//------------------------------------------------
//-----------------------Main---------------------
//------------------------------------------------
int __attribute__((noreturn)) main(void)
{
	uint8_t delay_counter=0;
	uint16_t voltage=0;

// Crystal Oscillator division factor: 1
// 	CLKPR=(1<<CLKPCE);
// 	CLKPR=(0<<CLKPCE) | (0<<CLKPS3) | (0<<CLKPS2) | (0<<CLKPS1) | (0<<CLKPS0);
	
//---------------------init-----------------------
	// ��������� ������ ��� ds
	DDRB |= _BV(DS_BIT);
	PORTB &= ~_BV(DS_BIT);
	DDRB &= ~_BV(DS_BIT);

	// ������������ Radio Control Node
 	DDRB |= _BV(RC_BIT);	

	// ������������� ��������
	// Watchdog Timer initialization
	// Watchdog Timer Prescaler: OSC/1024k
	// Watchdog timeout action: Interrupt
	WDTCR=(0<<WDTIF) | (0<<WDTIE) | (1<<WDP3) | (1<<WDCE) | (0<<WDE) | (0<<WDP2) | (0<<WDP1) | (1<<WDP0);
	WDTCR=(1<<WDTIF) | (1<<WDTIE) | (1<<WDP3) | (0<<WDCE) | (0<<WDE) | (0<<WDP2) | (0<<WDP1) | (1<<WDP0);
// 	wdt_enable(WDTO_2S);	// ���������� �������� ��� � 120��
 	WDTCR |= _BV(WDTIE);		// ���������� ���������� �� �������� (����� ����� �����)!
 	wdt_reset();			// �����

	// ������������ ���
	// ADC initialization
	// ADC Clock frequency: 605,000 kHz
	// ADC Bandgap Voltage Reference: On
	// ADC Auto Trigger Source: ADC Stopped
	// Digital input buffers on ADC0: On, ADC1: On, ADC2: On, ADC3: On
	DIDR0|=(0<<ADC0D) | (0<<ADC2D) | (0<<ADC3D) | (0<<ADC1D);
	ADMUX=ADC_VREF_TYPE;
	ADCSRA=(1<<ADEN) | (0<<ADSC) | (0<<ADATE) | (0<<ADIF) | (0<<ADIE) | (1<<ADPS2) | (0<<ADPS1) | (0<<ADPS0);
	ADCSRB=(0<<ADTS2) | (0<<ADTS1) | (0<<ADTS0);

 	sei();	// ���������� ����������

	// ������������� ������ ���
	set_sleep_mode (SLEEP_MODE_PWR_DOWN);
//---------------------init-----------------------

 	DDRB |= _BV(RC_BIT);
 	PORTB |= _BV(RC_BIT);


//---------------------main event loop--------------------- 
	for(;;)
	{                
//		_delay_ms(5000);
		
		if (delay_counter==0) 
		{
			delay_counter=timer;

			// ds			
			uint8_t SignBit;
			uint8_t DSdata[2];
			
			
			OneWireReset();
			OneWireOutByte(0xcc);
			OneWireOutByte(0x44);
			
			PORTB |= _BV(DS_BIT);
			DDRB |= _BV(DS_BIT);
			
			//  ���� ����� ������ ��������� �����������.
//			_delay_ms(1000); 
 			wdt_reset();
 			sleep_enable();   // ���������� ������ ���
 			sleep_cpu();   // ��������� ������ ���
			 
			DDRB &= ~_BV(DS_BIT);
			PORTB &= ~_BV(DS_BIT);
			
			
			OneWireReset();
			OneWireOutByte(0xcc);
			OneWireOutByte(0xbe);

			DSdata[0] = OneWireInByte();
			DSdata[1] = OneWireInByte();
			

			int TReading = (int)(DSdata[1] << 8) + DSdata[0];
			
			SignBit = TReading & 0x8000;
			if (SignBit) TReading = (TReading ^ 0xffff) + 1;
			
//			sendRC(SENDTCOM, ((6 * TReading) + TReading / 4)/10+500+keyT); // ���������� ������
			sendRC(SENDTCOM, ((6 * TReading) + TReading / 4)/10+1000); // ���������� ������
//			sendRC(SENDTCOM, TReading); // ���������� ������
			
			voltage=(uint16_t)read_adc(ADC_IN);
			sendRC(SENDVCOM, voltage); // ���������� ������
		}
		
 		wdt_reset();
 		sleep_enable();   // ���������� ������ ���
 		sleep_cpu();   // ��������� ������ ���
		delay_counter--;		
	}
//---------------------main event loop---------------------
	
}
//------------------------------------------------
//-----------------------Main---------------------
//------------------------------------------------
