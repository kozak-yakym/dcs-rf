#ifndef CONF_BOARD_H
#define CONF_BOARD_H

#include <mega8.h>
#include <delay.h>

#define F_CPU 16000000UL //for I2C

#define LED_CONF DDRB.0=1
#define YEL_LED_OFF PORTB.0=0
#define YEL_LED_ON PORTB.0=1
#define YEL_LED_TGL PORTB.0=!PORTB.1

#define RF_IN_CONF  DDRD.2=0
#define RF_IN_PIN   PIND.2 

#define INT0EB 6
#define INT1EB 7
#define EXT_INT0_EN GICR|=(1<<INT0EB)
#define EXT_INT0_DIS GICR&=~(1<<INT0EB)



#endif //CONF_BOARD_H