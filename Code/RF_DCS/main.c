/*****************************************************
This program was produced by the
CodeWizardAVR V2.03.4 Standard
Automatic Program Generator
� Copyright 1998-2008 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project : 
Version : 
Date    : 14.10.2015
Author  : 
Company : 
Comments: 


Chip type           : ATmega8
Program type        : Application
Clock frequency     : 16,000000 MHz
Memory model        : Small
External RAM size   : 0
Data Stack size     : 256
*****************************************************/

#include <mega8.h>
#include <stdio.h>
#include "conf_board.h"
#include "init_hw.h"
#include "rf_module.h"




void main(void)
{
    hw_init();
    printf("Started...\n\r");    
    YEL_LED_ON;
    delay_ms(100);
    YEL_LED_OFF;
    delay_ms(1000);
    
    printf("Enable INT0\n\r");    
    rf_enable();
    
    while (1)
          {
          
            YEL_LED_OFF;  //DEBUG
            delay_ms(100);

          };
}
