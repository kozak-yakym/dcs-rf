
;CodeVisionAVR C Compiler V2.03.4 Standard
;(C) Copyright 1998-2008 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com

;Chip type              : ATmega8
;Program type           : Application
;Clock frequency        : 16,000000 MHz
;Memory model           : Small
;Optimize for           : Size
;(s)printf features     : int, width
;(s)scanf features      : int, width
;External RAM size      : 0
;Data Stack size        : 256 byte(s)
;Heap size              : 0 byte(s)
;Promote char to int    : Yes
;char is unsigned       : Yes
;global const stored in FLASH  : No
;8 bit enums            : Yes
;Enhanced core instructions    : On
;Smart register allocation : On
;Automatic register allocation : On

	#pragma AVRPART ADMIN PART_NAME ATmega8
	#pragma AVRPART MEMORY PROG_FLASH 8192
	#pragma AVRPART MEMORY EEPROM 512
	#pragma AVRPART MEMORY INT_SRAM SIZE 1024
	#pragma AVRPART MEMORY INT_SRAM START_ADDR 0x60

	.LISTMAC
	.EQU UDRE=0x5
	.EQU RXC=0x7
	.EQU USR=0xB
	.EQU UDR=0xC
	.EQU SPSR=0xE
	.EQU SPDR=0xF
	.EQU EERE=0x0
	.EQU EEWE=0x1
	.EQU EEMWE=0x2
	.EQU EECR=0x1C
	.EQU EEDR=0x1D
	.EQU EEARL=0x1E
	.EQU EEARH=0x1F
	.EQU WDTCR=0x21
	.EQU MCUCR=0x35
	.EQU GICR=0x3B
	.EQU SPL=0x3D
	.EQU SPH=0x3E
	.EQU SREG=0x3F

	.DEF R0X0=R0
	.DEF R0X1=R1
	.DEF R0X2=R2
	.DEF R0X3=R3
	.DEF R0X4=R4
	.DEF R0X5=R5
	.DEF R0X6=R6
	.DEF R0X7=R7
	.DEF R0X8=R8
	.DEF R0X9=R9
	.DEF R0XA=R10
	.DEF R0XB=R11
	.DEF R0XC=R12
	.DEF R0XD=R13
	.DEF R0XE=R14
	.DEF R0XF=R15
	.DEF R0X10=R16
	.DEF R0X11=R17
	.DEF R0X12=R18
	.DEF R0X13=R19
	.DEF R0X14=R20
	.DEF R0X15=R21
	.DEF R0X16=R22
	.DEF R0X17=R23
	.DEF R0X18=R24
	.DEF R0X19=R25
	.DEF R0X1A=R26
	.DEF R0X1B=R27
	.DEF R0X1C=R28
	.DEF R0X1D=R29
	.DEF R0X1E=R30
	.DEF R0X1F=R31

	.MACRO __CPD1N
	CPI  R30,LOW(@0)
	LDI  R26,HIGH(@0)
	CPC  R31,R26
	LDI  R26,BYTE3(@0)
	CPC  R22,R26
	LDI  R26,BYTE4(@0)
	CPC  R23,R26
	.ENDM

	.MACRO __CPD2N
	CPI  R26,LOW(@0)
	LDI  R30,HIGH(@0)
	CPC  R27,R30
	LDI  R30,BYTE3(@0)
	CPC  R24,R30
	LDI  R30,BYTE4(@0)
	CPC  R25,R30
	.ENDM

	.MACRO __CPWRR
	CP   R@0,R@2
	CPC  R@1,R@3
	.ENDM

	.MACRO __CPWRN
	CPI  R@0,LOW(@2)
	LDI  R30,HIGH(@2)
	CPC  R@1,R30
	.ENDM

	.MACRO __ADDB1MN
	SUBI R30,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDB2MN
	SUBI R26,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDW1MN
	SUBI R30,LOW(-@0-(@1))
	SBCI R31,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW2MN
	SUBI R26,LOW(-@0-(@1))
	SBCI R27,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	SBCI R22,BYTE3(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1N
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	SBCI R22,BYTE3(-@0)
	SBCI R23,BYTE4(-@0)
	.ENDM

	.MACRO __ADDD2N
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	SBCI R24,BYTE3(-@0)
	SBCI R25,BYTE4(-@0)
	.ENDM

	.MACRO __SUBD1N
	SUBI R30,LOW(@0)
	SBCI R31,HIGH(@0)
	SBCI R22,BYTE3(@0)
	SBCI R23,BYTE4(@0)
	.ENDM

	.MACRO __SUBD2N
	SUBI R26,LOW(@0)
	SBCI R27,HIGH(@0)
	SBCI R24,BYTE3(@0)
	SBCI R25,BYTE4(@0)
	.ENDM

	.MACRO __ANDBMNN
	LDS  R30,@0+@1
	ANDI R30,LOW(@2)
	STS  @0+@1,R30
	.ENDM

	.MACRO __ANDWMNN
	LDS  R30,@0+@1
	ANDI R30,LOW(@2)
	STS  @0+@1,R30
	LDS  R30,@0+@1+1
	ANDI R30,HIGH(@2)
	STS  @0+@1+1,R30
	.ENDM

	.MACRO __ANDD1N
	ANDI R30,LOW(@0)
	ANDI R31,HIGH(@0)
	ANDI R22,BYTE3(@0)
	ANDI R23,BYTE4(@0)
	.ENDM

	.MACRO __ANDD2N
	ANDI R26,LOW(@0)
	ANDI R27,HIGH(@0)
	ANDI R24,BYTE3(@0)
	ANDI R25,BYTE4(@0)
	.ENDM

	.MACRO __ORBMNN
	LDS  R30,@0+@1
	ORI  R30,LOW(@2)
	STS  @0+@1,R30
	.ENDM

	.MACRO __ORWMNN
	LDS  R30,@0+@1
	ORI  R30,LOW(@2)
	STS  @0+@1,R30
	LDS  R30,@0+@1+1
	ORI  R30,HIGH(@2)
	STS  @0+@1+1,R30
	.ENDM

	.MACRO __ORD1N
	ORI  R30,LOW(@0)
	ORI  R31,HIGH(@0)
	ORI  R22,BYTE3(@0)
	ORI  R23,BYTE4(@0)
	.ENDM

	.MACRO __ORD2N
	ORI  R26,LOW(@0)
	ORI  R27,HIGH(@0)
	ORI  R24,BYTE3(@0)
	ORI  R25,BYTE4(@0)
	.ENDM

	.MACRO __DELAY_USB
	LDI  R24,LOW(@0)
__DELAY_USB_LOOP:
	DEC  R24
	BRNE __DELAY_USB_LOOP
	.ENDM

	.MACRO __DELAY_USW
	LDI  R24,LOW(@0)
	LDI  R25,HIGH(@0)
__DELAY_USW_LOOP:
	SBIW R24,1
	BRNE __DELAY_USW_LOOP
	.ENDM

	.MACRO __CLRD1S
	LDI  R30,0
	STD  Y+@0,R30
	STD  Y+@0+1,R30
	STD  Y+@0+2,R30
	STD  Y+@0+3,R30
	.ENDM

	.MACRO __GETD1S
	LDD  R30,Y+@0
	LDD  R31,Y+@0+1
	LDD  R22,Y+@0+2
	LDD  R23,Y+@0+3
	.ENDM

	.MACRO __PUTD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R31
	STD  Y+@0+2,R22
	STD  Y+@0+3,R23
	.ENDM

	.MACRO __PUTD2S
	STD  Y+@0,R26
	STD  Y+@0+1,R27
	STD  Y+@0+2,R24
	STD  Y+@0+3,R25
	.ENDM

	.MACRO __POINTB1MN
	LDI  R30,LOW(@0+@1)
	.ENDM

	.MACRO __POINTW1MN
	LDI  R30,LOW(@0+@1)
	LDI  R31,HIGH(@0+@1)
	.ENDM

	.MACRO __POINTD1M
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __POINTW1FN
	LDI  R30,LOW(2*@0+@1)
	LDI  R31,HIGH(2*@0+@1)
	.ENDM

	.MACRO __POINTD1FN
	LDI  R30,LOW(2*@0+@1)
	LDI  R31,HIGH(2*@0+@1)
	LDI  R22,BYTE3(2*@0+@1)
	LDI  R23,BYTE4(2*@0+@1)
	.ENDM

	.MACRO __POINTB2MN
	LDI  R26,LOW(@0+@1)
	.ENDM

	.MACRO __POINTW2MN
	LDI  R26,LOW(@0+@1)
	LDI  R27,HIGH(@0+@1)
	.ENDM

	.MACRO __POINTBRM
	LDI  R@0,LOW(@1)
	.ENDM

	.MACRO __POINTWRM
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __POINTBRMN
	LDI  R@0,LOW(@1+@2)
	.ENDM

	.MACRO __POINTWRMN
	LDI  R@0,LOW(@2+@3)
	LDI  R@1,HIGH(@2+@3)
	.ENDM

	.MACRO __POINTWRFN
	LDI  R@0,LOW(@2*2+@3)
	LDI  R@1,HIGH(@2*2+@3)
	.ENDM

	.MACRO __GETD1N
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __GETD2N
	LDI  R26,LOW(@0)
	LDI  R27,HIGH(@0)
	LDI  R24,BYTE3(@0)
	LDI  R25,BYTE4(@0)
	.ENDM

	.MACRO __GETD2S
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	LDD  R24,Y+@0+2
	LDD  R25,Y+@0+3
	.ENDM

	.MACRO __GETB1MN
	LDS  R30,@0+@1
	.ENDM

	.MACRO __GETB1HMN
	LDS  R31,@0+@1
	.ENDM

	.MACRO __GETW1MN
	LDS  R30,@0+@1
	LDS  R31,@0+@1+1
	.ENDM

	.MACRO __GETD1MN
	LDS  R30,@0+@1
	LDS  R31,@0+@1+1
	LDS  R22,@0+@1+2
	LDS  R23,@0+@1+3
	.ENDM

	.MACRO __GETBRMN
	LDS  R@0,@1+@2
	.ENDM

	.MACRO __GETWRMN
	LDS  R@0,@2+@3
	LDS  R@1,@2+@3+1
	.ENDM

	.MACRO __GETWRZ
	LDD  R@0,Z+@2
	LDD  R@1,Z+@2+1
	.ENDM

	.MACRO __GETD2Z
	LDD  R26,Z+@0
	LDD  R27,Z+@0+1
	LDD  R24,Z+@0+2
	LDD  R25,Z+@0+3
	.ENDM

	.MACRO __GETB2MN
	LDS  R26,@0+@1
	.ENDM

	.MACRO __GETW2MN
	LDS  R26,@0+@1
	LDS  R27,@0+@1+1
	.ENDM

	.MACRO __GETD2MN
	LDS  R26,@0+@1
	LDS  R27,@0+@1+1
	LDS  R24,@0+@1+2
	LDS  R25,@0+@1+3
	.ENDM

	.MACRO __PUTB1MN
	STS  @0+@1,R30
	.ENDM

	.MACRO __PUTW1MN
	STS  @0+@1,R30
	STS  @0+@1+1,R31
	.ENDM

	.MACRO __PUTD1MN
	STS  @0+@1,R30
	STS  @0+@1+1,R31
	STS  @0+@1+2,R22
	STS  @0+@1+3,R23
	.ENDM

	.MACRO __PUTB1EN
	LDI  R26,LOW(@0+@1)
	LDI  R27,HIGH(@0+@1)
	RCALL __EEPROMWRB
	.ENDM

	.MACRO __PUTW1EN
	LDI  R26,LOW(@0+@1)
	LDI  R27,HIGH(@0+@1)
	RCALL __EEPROMWRW
	.ENDM

	.MACRO __PUTD1EN
	LDI  R26,LOW(@0+@1)
	LDI  R27,HIGH(@0+@1)
	RCALL __EEPROMWRD
	.ENDM

	.MACRO __PUTBR0MN
	STS  @0+@1,R0
	.ENDM

	.MACRO __PUTDZ2
	STD  Z+@0,R26
	STD  Z+@0+1,R27
	STD  Z+@0+2,R24
	STD  Z+@0+3,R25
	.ENDM

	.MACRO __PUTBMRN
	STS  @0+@1,R@2
	.ENDM

	.MACRO __PUTWMRN
	STS  @0+@1,R@2
	STS  @0+@1+1,R@3
	.ENDM

	.MACRO __PUTBZR
	STD  Z+@1,R@0
	.ENDM

	.MACRO __PUTWZR
	STD  Z+@2,R@0
	STD  Z+@2+1,R@1
	.ENDM

	.MACRO __GETW1R
	MOV  R30,R@0
	MOV  R31,R@1
	.ENDM

	.MACRO __GETW2R
	MOV  R26,R@0
	MOV  R27,R@1
	.ENDM

	.MACRO __GETWRN
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __PUTW1R
	MOV  R@0,R30
	MOV  R@1,R31
	.ENDM

	.MACRO __PUTW2R
	MOV  R@0,R26
	MOV  R@1,R27
	.ENDM

	.MACRO __ADDWRN
	SUBI R@0,LOW(-@2)
	SBCI R@1,HIGH(-@2)
	.ENDM

	.MACRO __ADDWRR
	ADD  R@0,R@2
	ADC  R@1,R@3
	.ENDM

	.MACRO __SUBWRN
	SUBI R@0,LOW(@2)
	SBCI R@1,HIGH(@2)
	.ENDM

	.MACRO __SUBWRR
	SUB  R@0,R@2
	SBC  R@1,R@3
	.ENDM

	.MACRO __ANDWRN
	ANDI R@0,LOW(@2)
	ANDI R@1,HIGH(@2)
	.ENDM

	.MACRO __ANDWRR
	AND  R@0,R@2
	AND  R@1,R@3
	.ENDM

	.MACRO __ORWRN
	ORI  R@0,LOW(@2)
	ORI  R@1,HIGH(@2)
	.ENDM

	.MACRO __ORWRR
	OR   R@0,R@2
	OR   R@1,R@3
	.ENDM

	.MACRO __EORWRR
	EOR  R@0,R@2
	EOR  R@1,R@3
	.ENDM

	.MACRO __GETWRS
	LDD  R@0,Y+@2
	LDD  R@1,Y+@2+1
	.ENDM

	.MACRO __PUTWSR
	STD  Y+@2,R@0
	STD  Y+@2+1,R@1
	.ENDM

	.MACRO __MOVEWRR
	MOV  R@0,R@2
	MOV  R@1,R@3
	.ENDM

	.MACRO __INWR
	IN   R@0,@2
	IN   R@1,@2+1
	.ENDM

	.MACRO __OUTWR
	OUT  @2+1,R@1
	OUT  @2,R@0
	.ENDM

	.MACRO __CALL1MN
	LDS  R30,@0+@1
	LDS  R31,@0+@1+1
	ICALL
	.ENDM

	.MACRO __CALL1FN
	LDI  R30,LOW(2*@0+@1)
	LDI  R31,HIGH(2*@0+@1)
	RCALL __GETW1PF
	ICALL
	.ENDM

	.MACRO __CALL2EN
	LDI  R26,LOW(@0+@1)
	LDI  R27,HIGH(@0+@1)
	RCALL __EEPROMRDW
	ICALL
	.ENDM

	.MACRO __GETW1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X
	.ENDM

	.MACRO __NBST
	BST  R@0,@1
	IN   R30,SREG
	LDI  R31,0x40
	EOR  R30,R31
	OUT  SREG,R30
	.ENDM


	.MACRO __PUTB1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RNS
	MOVW R26,R@0
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	RCALL __PUTDP1
	.ENDM


	.MACRO __GETB1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R30,Z
	.ENDM

	.MACRO __GETB1HSX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	.ENDM

	.MACRO __GETW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R31,Z
	MOV  R30,R0
	.ENDM

	.MACRO __GETD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R1,Z+
	LD   R22,Z+
	LD   R23,Z
	MOVW R30,R0
	.ENDM

	.MACRO __GETB2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R26,X
	.ENDM

	.MACRO __GETW2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	.ENDM

	.MACRO __GETD2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R1,X+
	LD   R24,X+
	LD   R25,X
	MOVW R26,R0
	.ENDM

	.MACRO __GETBRSX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	LD   R@0,Z
	.ENDM

	.MACRO __GETWRSX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	LD   R@0,Z+
	LD   R@1,Z
	.ENDM

	.MACRO __LSLW8SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	CLR  R30
	.ENDM

	.MACRO __PUTB1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __CLRW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	CLR  R0
	ST   Z+,R0
	ST   Z,R0
	.ENDM

	.MACRO __CLRD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	CLR  R0
	ST   Z+,R0
	ST   Z+,R0
	ST   Z+,R0
	ST   Z,R0
	.ENDM

	.MACRO __PUTB2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R26
	.ENDM

	.MACRO __PUTW2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z,R27
	.ENDM

	.MACRO __PUTD2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z+,R27
	ST   Z+,R24
	ST   Z,R25
	.ENDM

	.MACRO __PUTBSRX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R@1
	.ENDM

	.MACRO __PUTWSRX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	ST   Z+,R@0
	ST   Z,R@1
	.ENDM

	.MACRO __PUTB1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __MULBRR
	MULS R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRRU
	MUL  R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRR0
	MULS R@0,R@1
	.ENDM

	.MACRO __MULBRRU0
	MUL  R@0,R@1
	.ENDM

	.MACRO __MULBNWRU
	LDI  R26,@2
	MUL  R26,R@0
	MOVW R30,R0
	MUL  R26,R@1
	ADD  R31,R0
	.ENDM

;NAME DEFINITIONS FOR GLOBAL VARIABLES ALLOCATED TO REGISTERS
	.DEF _rf_capture_cnt=R5
	.DEF _rf_repeat_cnt=R6
	.DEF _t_half_1_bit=R8
	.DEF _t_half_2_bit=R10
	.DEF _delta_bit=R12

	.CSEG
	.ORG 0x00

;INTERRUPT VECTORS
	RJMP __RESET
	RJMP _ext_int0_isr
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP _timer1_ovf_isr
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00

_tbl10_G100:
	.DB  0x10,0x27,0xE8,0x3,0x64,0x0,0xA,0x0
	.DB  0x1,0x0
_tbl16_G100:
	.DB  0x0,0x10,0x0,0x1,0x10,0x0,0x1,0x0

_0x20000:
	.DB  0x53,0x74,0x61,0x72,0x74,0x65,0x64,0x2E
	.DB  0x2E,0x2E,0xA,0xD,0x0,0x45,0x6E,0x61
	.DB  0x62,0x6C,0x65,0x20,0x49,0x4E,0x54,0x30
	.DB  0xA,0xD,0x0
_0x4004E:
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x3
	.DB  0x0
_0x40000:
	.DB  0xA,0x0,0x49,0x44,0x3A,0x30,0x78,0x25
	.DB  0x30,0x34,0x58,0x20,0x0,0x43,0x4F,0x4D
	.DB  0x4D,0x41,0x4E,0x44,0x3A,0x30,0x78,0x25
	.DB  0x30,0x32,0x58,0x20,0x0,0x74,0x76,0x61
	.DB  0x6C,0x3D,0x25,0x30,0x34,0x64,0x20,0x0
	.DB  0x74,0x64,0x65,0x67,0x3D,0x25,0x30,0x33
	.DB  0x64,0x0,0x75,0x76,0x61,0x6C,0x3D,0x25
	.DB  0x30,0x34,0x64,0x20,0x0,0x75,0x76,0x6F
	.DB  0x6C,0x74,0x3D,0x25,0x30,0x33,0x64,0x0

__GLOBAL_INI_TBL:
	.DW  0x09
	.DW  0x05
	.DW  _0x4004E*2

_0xFFFFFFFF:
	.DW  0

__RESET:
	CLI
	CLR  R30
	OUT  EECR,R30

;INTERRUPT VECTORS ARE PLACED
;AT THE START OF FLASH
	LDI  R31,1
	OUT  GICR,R31
	OUT  GICR,R30
	OUT  MCUCR,R30

;DISABLE WATCHDOG
	LDI  R31,0x18
	OUT  WDTCR,R31
	OUT  WDTCR,R30

;CLEAR R2-R14
	LDI  R24,(14-2)+1
	LDI  R26,2
	CLR  R27
__CLEAR_REG:
	ST   X+,R30
	DEC  R24
	BRNE __CLEAR_REG

;CLEAR SRAM
	LDI  R24,LOW(0x400)
	LDI  R25,HIGH(0x400)
	LDI  R26,0x60
__CLEAR_SRAM:
	ST   X+,R30
	SBIW R24,1
	BRNE __CLEAR_SRAM

;GLOBAL VARIABLES INITIALIZATION
	LDI  R30,LOW(__GLOBAL_INI_TBL*2)
	LDI  R31,HIGH(__GLOBAL_INI_TBL*2)
__GLOBAL_INI_NEXT:
	LPM  R24,Z+
	LPM  R25,Z+
	SBIW R24,0
	BREQ __GLOBAL_INI_END
	LPM  R26,Z+
	LPM  R27,Z+
	LPM  R0,Z+
	LPM  R1,Z+
	MOVW R22,R30
	MOVW R30,R0
__GLOBAL_INI_LOOP:
	LPM  R0,Z+
	ST   X+,R0
	SBIW R24,1
	BRNE __GLOBAL_INI_LOOP
	MOVW R30,R22
	RJMP __GLOBAL_INI_NEXT
__GLOBAL_INI_END:

;STACK POINTER INITIALIZATION
	LDI  R30,LOW(0x45F)
	OUT  SPL,R30
	LDI  R30,HIGH(0x45F)
	OUT  SPH,R30

;DATA STACK POINTER INITIALIZATION
	LDI  R28,LOW(0x160)
	LDI  R29,HIGH(0x160)

	RJMP _main

	.ESEG
	.ORG 0

	.DSEG
	.ORG 0x160

	.CSEG
;#include <mega8.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;#include <delay.h>
;#include "conf_board.h"
;#include "init_hw.h"
;
;
;void hw_init(void)
; 0000 0008 {

	.CSEG
_hw_init:
; 0000 0009     PORTB=0x00;
	LDI  R30,LOW(0)
	OUT  0x18,R30
; 0000 000A     DDRB=0x00;
	OUT  0x17,R30
; 0000 000B     PORTC=0x00;
	OUT  0x15,R30
; 0000 000C     DDRC=0x00;
	OUT  0x14,R30
; 0000 000D     PORTD=0x00;
	OUT  0x12,R30
; 0000 000E     DDRD=0x00;
	OUT  0x11,R30
; 0000 000F 
; 0000 0010     RF_IN_CONF;
	CBI  0x11,2
; 0000 0011     LED_CONF;
	SBI  0x17,0
; 0000 0012 
; 0000 0013     // Timer/Counter 0 initialization
; 0000 0014     // Clock source: System Clock
; 0000 0015     // Clock value: Timer 0 Stopped
; 0000 0016     TCCR0=0x00;
	OUT  0x33,R30
; 0000 0017     TCNT0=0x00;
	OUT  0x32,R30
; 0000 0018 
; 0000 0019     // Timer/Counter 1 initialization
; 0000 001A     // Clock source: System Clock
; 0000 001B     //    // Clock value: 16000,000 kHz
; 0000 001C     // Clock value: 2000,000 kHz
; 0000 001D     // Mode: Normal top=FFFFh
; 0000 001E     // OC1A output: Discon.
; 0000 001F     // OC1B output: Discon.
; 0000 0020     // Noise Canceler: Off
; 0000 0021     // Input Capture on Falling Edge
; 0000 0022     // Timer 1 Overflow Interrupt: On
; 0000 0023     // Input Capture Interrupt: Off
; 0000 0024     // Compare A Match Interrupt: Off
; 0000 0025     // Compare B Match Interrupt: Off
; 0000 0026     TCCR1A=0x00;
	OUT  0x2F,R30
; 0000 0027     TCCR1B=0x01;
	LDI  R30,LOW(1)
	OUT  0x2E,R30
; 0000 0028 //    TCCR1B=0x02;
; 0000 0029     TCNT1H=0x00;
	LDI  R30,LOW(0)
	OUT  0x2D,R30
; 0000 002A     TCNT1L=0x00;
	OUT  0x2C,R30
; 0000 002B     ICR1H=0x00;
	OUT  0x27,R30
; 0000 002C     ICR1L=0x00;
	OUT  0x26,R30
; 0000 002D     OCR1AH=0x00;
	OUT  0x2B,R30
; 0000 002E     OCR1AL=0x00;
	OUT  0x2A,R30
; 0000 002F     OCR1BH=0x00;
	OUT  0x29,R30
; 0000 0030     OCR1BL=0x00;
	OUT  0x28,R30
; 0000 0031 
; 0000 0032     // Timer/Counter 2 initialization
; 0000 0033     // Clock source: System Clock
; 0000 0034     // Clock value: Timer 2 Stopped
; 0000 0035     // Mode: Normal top=FFh
; 0000 0036     // OC2 output: Disconnected
; 0000 0037     ASSR=0x00;
	OUT  0x22,R30
; 0000 0038     TCCR2=0x00;
	OUT  0x25,R30
; 0000 0039     TCNT2=0x00;
	OUT  0x24,R30
; 0000 003A     OCR2=0x00;
	OUT  0x23,R30
; 0000 003B 
; 0000 003C     // External Interrupt(s) initialization
; 0000 003D     // INT0: On
; 0000 003E     // INT0 Mode: Any change
; 0000 003F     // INT1: Off
; 0000 0040     EXT_INT0_DIS;
	RCALL SUBOPT_0x0
	ANDI R30,LOW(0xFFBF)
	OUT  0x3B,R30
; 0000 0041 //    EXT_INT0_EN; //GICR|=0x40;
; 0000 0042     GICR|=0x00;
	RCALL SUBOPT_0x0
	OUT  0x3B,R30
; 0000 0043     MCUCR=0x01;
	LDI  R30,LOW(1)
	OUT  0x35,R30
; 0000 0044     GIFR=0x40;
	LDI  R30,LOW(64)
	OUT  0x3A,R30
; 0000 0045 
; 0000 0046     // Timer(s)/Counter(s) Interrupt(s) initialization
; 0000 0047     TIMSK=0x04;
	LDI  R30,LOW(4)
	OUT  0x39,R30
; 0000 0048 
; 0000 0049     // USART initialization
; 0000 004A     // Communication Parameters: 8 Data, 1 Stop, No Parity
; 0000 004B     // USART Receiver: Off
; 0000 004C     // USART Transmitter: On
; 0000 004D     // USART Mode: Asynchronous
; 0000 004E     // USART Baud Rate: 9600
; 0000 004F     UCSRA=0x00;
	LDI  R30,LOW(0)
	OUT  0xB,R30
; 0000 0050     UCSRB=0x08;
	LDI  R30,LOW(8)
	OUT  0xA,R30
; 0000 0051     UCSRC=0x86;
	LDI  R30,LOW(134)
	OUT  0x20,R30
; 0000 0052     UBRRH=0x00;
	LDI  R30,LOW(0)
	OUT  0x20,R30
; 0000 0053     UBRRL=0x67;
	LDI  R30,LOW(103)
	OUT  0x9,R30
; 0000 0054 
; 0000 0055     // Analog Comparator initialization
; 0000 0056     // Analog Comparator: Off
; 0000 0057     // Analog Comparator Input Capture by Timer/Counter 1: Off
; 0000 0058     ACSR=0x80;
	LDI  R30,LOW(128)
	OUT  0x8,R30
; 0000 0059     SFIOR=0x00;
	LDI  R30,LOW(0)
	OUT  0x30,R30
; 0000 005A 
; 0000 005B     // Global enable interrupts
; 0000 005C     #asm("sei")
	sei
; 0000 005D 
; 0000 005E }
	RET
;
;
;
;
;
;
;
;
;
;/*****************************************************
;This program was produced by the
;CodeWizardAVR V2.03.4 Standard
;Automatic Program Generator
;� Copyright 1998-2008 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com
;
;Project :
;Version :
;Date    : 14.10.2015
;Author  :
;Company :
;Comments:
;
;
;Chip type           : ATmega8
;Program type        : Application
;Clock frequency     : 16,000000 MHz
;Memory model        : Small
;External RAM size   : 0
;Data Stack size     : 256
;*****************************************************/
;
;#include <mega8.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;#include <stdio.h>
;#include "conf_board.h"
;#include "init_hw.h"
;#include "rf_module.h"
;
;
;
;
;void main(void)
; 0001 0022 {

	.CSEG
_main:
; 0001 0023     hw_init();
	RCALL _hw_init
; 0001 0024     printf("Started...\n\r");
	__POINTW1FN _0x20000,0
	RCALL SUBOPT_0x1
	RCALL SUBOPT_0x2
; 0001 0025     YEL_LED_ON;
	SBI  0x18,0
; 0001 0026     delay_ms(100);
	RCALL SUBOPT_0x3
; 0001 0027     YEL_LED_OFF;
	CBI  0x18,0
; 0001 0028     delay_ms(1000);
	LDI  R30,LOW(1000)
	LDI  R31,HIGH(1000)
	RCALL SUBOPT_0x1
	RCALL _delay_ms
; 0001 0029 
; 0001 002A     printf("Enable INT0\n\r");
	__POINTW1FN _0x20000,13
	RCALL SUBOPT_0x1
	RCALL SUBOPT_0x2
; 0001 002B     rf_enable();
	RCALL _rf_enable
; 0001 002C 
; 0001 002D     while (1)
_0x20007:
; 0001 002E           {
; 0001 002F 
; 0001 0030             YEL_LED_OFF;  //DEBUG
	CBI  0x18,0
; 0001 0031             delay_ms(100);
	RCALL SUBOPT_0x3
; 0001 0032 
; 0001 0033           };
	RJMP _0x20007
; 0001 0034 }
_0x2000C:
	RJMP _0x2000C
;#include <mega8.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;#include <stdio.h>
;#include "rf_module.h"
;#include "conf_board.h"
;#include "init_hw.h"
;#include "c_types.h"
;
;
;//uint8_t capture_edge=CCP_CAPTURE_RE;
;uint8_t rf_capture_array[KEY_BITS]; //int1
;uint8_t rf_capture_array_prev[KEY_BITS]; //int1
;uint8_t rf_capture_cnt=0;
;uint16_t rf_repeat_cnt=0;
;uint8_t bit_8_capt_arr[6];
;
;// bit consist of 2 parts one part bigger in 3 times than another
;uint16_t t_half_1_bit=0;
;uint16_t t_half_2_bit=0;
;// Coefficient which give maximum value of differense between signal pulses
;// calculates as 100%/x% and must divide value for calculating percentage
;uint16_t delta_bit=3;
;
;
;uint16_t keychain_status=0;
;uint16_t keychain_short_status=0;
;
;uint16_t openkey_sw_cnt=0;
;
;uint16_t prnt_cnt=0;
;
;uint16_t ovf_t1_cnt=0;
;
;
;uint16_t get_timer1(void)
; 0002 0023 {

	.CSEG
_get_timer1:
; 0002 0024     return TCNT1;
	IN   R30,0x2C
	IN   R31,0x2C+1
	RET
; 0002 0025 }
;
;void set_timer1(uint16_t tmr1_val)
; 0002 0028 {
_set_timer1:
; 0002 0029     TCNT1=tmr1_val;
;	tmr1_val -> Y+0
	LD   R30,Y
	LDD  R31,Y+1
	OUT  0x2C+1,R31
	OUT  0x2C,R30
; 0002 002A }
	ADIW R28,2
	RET
;
;//stop code accepting
;void rf_disable(void)
; 0002 002E {
_rf_disable:
; 0002 002F     EXT_INT0_DIS;
	RCALL SUBOPT_0x0
	ANDI R30,LOW(0xFFBF)
	RJMP _0x2060001
; 0002 0030 }
;
;//start code accepting
;void rf_enable(void)
; 0002 0034 {
_rf_enable:
; 0002 0035     EXT_INT0_EN;
	RCALL SUBOPT_0x0
	ORI  R30,0x40
_0x2060001:
	OUT  0x3B,R30
; 0002 0036 }
	RET
;
;uint16_t pressed_keychain_status(void)
; 0002 0039 {
; 0002 003A     return keychain_status;
; 0002 003B }
;
;void pressed_keychain_status_clear(void)
; 0002 003E {
; 0002 003F     keychain_status=0;
; 0002 0040     keychain_short_status=0;
; 0002 0041 }
;
;//CRC-8 - based on the CRC8 formulas by Dallas/Maxim
;//code released under the therms of the GNU GPL 3.0 license
;unsigned char CRC8(const unsigned char *data, unsigned char len)
; 0002 0046 {
_CRC8:
; 0002 0047 	unsigned char crc = 0x00;
; 0002 0048 	while (len--)
	ST   -Y,R17
;	*data -> Y+2
;	len -> Y+1
;	crc -> R17
	LDI  R17,0
_0x40003:
	LDD  R30,Y+1
	SUBI R30,LOW(1)
	STD  Y+1,R30
	SUBI R30,-LOW(1)
	BREQ _0x40005
; 0002 0049 	{
; 0002 004A 		unsigned char extract = *data++;
; 0002 004B         unsigned char tempI;
; 0002 004C 		for (tempI = 8; tempI; tempI--)
	SBIW R28,2
;	*data -> Y+4
;	len -> Y+3
;	extract -> Y+1
;	tempI -> Y+0
	RCALL SUBOPT_0x4
	LD   R30,X+
	STD  Y+4,R26
	STD  Y+4+1,R27
	STD  Y+1,R30
	LDI  R30,LOW(8)
	ST   Y,R30
_0x40007:
	LD   R30,Y
	CPI  R30,0
	BREQ _0x40008
; 0002 004D 		{
; 0002 004E 			unsigned char sum = (crc ^ extract) & 0x01;
; 0002 004F 			crc >>= 1;
	SBIW R28,1
;	*data -> Y+5
;	len -> Y+4
;	extract -> Y+2
;	tempI -> Y+1
;	sum -> Y+0
	MOV  R26,R17
	CLR  R27
	RCALL SUBOPT_0x5
	EOR  R30,R26
	EOR  R31,R27
	ANDI R30,LOW(0x1)
	ST   Y,R30
	RCALL SUBOPT_0x6
	RCALL SUBOPT_0x7
	MOV  R17,R30
; 0002 0050 			if (sum)
	LD   R30,Y
	CPI  R30,0
	BREQ _0x40009
; 0002 0051 			{
; 0002 0052 				crc ^= 0x8C;
	RCALL SUBOPT_0x6
	LDI  R30,LOW(140)
	LDI  R31,HIGH(140)
	EOR  R30,R26
	MOV  R17,R30
; 0002 0053 			}
; 0002 0054 			extract >>= 1;
_0x40009:
	RCALL SUBOPT_0x5
	ASR  R31
	ROR  R30
	STD  Y+2,R30
; 0002 0055 		}
	ADIW R28,1
	LD   R30,Y
	SUBI R30,LOW(1)
	ST   Y,R30
	RJMP _0x40007
_0x40008:
; 0002 0056 	}
	ADIW R28,2
	RJMP _0x40003
_0x40005:
; 0002 0057 	return crc;
	MOV  R30,R17
	LDD  R17,Y+0
	ADIW R28,4
	RET
; 0002 0058 }
;
;
;//convert data from bool array to bytes array
;//void rf_data_convert(int1 *in_arr, unsigned int8 *out_arr)  //WHY Don't work???
;void rf_capture_convert(uint8_t *out_arr)
; 0002 005E {
; 0002 005F     uint8_t lng=0, lng_b=0;
; 0002 0060     //convert 24 bytes only, and no start bit
; 0002 0061     for(lng=0, lng_b=0; lng<(KEY_LENGTH*8); lng++)
;	*out_arr -> Y+2
;	lng -> R17
;	lng_b -> R16
; 0002 0062     {
; 0002 0063         if(lng%8==0)
; 0002 0064         {
; 0002 0065             if(lng!=0)
; 0002 0066             {
; 0002 0067                 lng_b++;
; 0002 0068             }
; 0002 0069             out_arr[lng_b]=0x00;
; 0002 006A         }
; 0002 006B 
; 0002 006C         if(rf_capture_array[lng+1])
; 0002 006D         {
; 0002 006E             out_arr[lng_b]|=((uint8_t)1<<(lng%8));
; 0002 006F //        out_arr[lng_b]|=(1<<(lng%8));
; 0002 0070         }
; 0002 0071     }
; 0002 0072 }
;
;
;// Timer 1 overflow interrupt service routine
;interrupt [TIM1_OVF] void timer1_ovf_isr(void)
; 0002 0077 {
_timer1_ovf_isr:
	ST   -Y,R26
	ST   -Y,R27
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
; 0002 0078     ovf_t1_cnt++;
	LDI  R26,LOW(_ovf_t1_cnt)
	LDI  R27,HIGH(_ovf_t1_cnt)
	RCALL SUBOPT_0x8
; 0002 0079 }
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	LD   R27,Y+
	LD   R26,Y+
	RETI
;
;
;// External Interrupt 0 service routine
;//interrupt [EXT_INT0] void ext_int0_isr(void)
;//{
;//    LED=!LED;
;//}
;
;//#INT_CCP1
;//void  CCP1_isr(void)
;interrupt [EXT_INT0] void ext_int0_isr(void)
; 0002 0085 {
_ext_int0_isr:
	ST   -Y,R0
	ST   -Y,R1
	ST   -Y,R15
	ST   -Y,R22
	ST   -Y,R23
	ST   -Y,R24
	ST   -Y,R25
	ST   -Y,R26
	ST   -Y,R27
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
; 0002 0086     uint32_t t_t_val=0;
; 0002 0087     uint16_t t1_val; //!=0;
; 0002 0088     uint16_t t_1_2_bit=0; //middle value for comparing hi and lo pulses
; 0002 0089     uint16_t t_1_2_delta=0; //middle value for calculating delta from short signal
; 0002 008A 
; 0002 008B     uint8_t rf_capt8_arr[KEY_LENGTH]; //array after transforming to 8bit
; 0002 008C 
; 0002 008D /*
; 0002 008E     if(capture_edge==CCP_CAPTURE_RE) //if previous Edge was Rising (low level)
; 0002 008F     {
; 0002 0090         t1_val=get_timer1();
; 0002 0091         set_timer1(0x00); //null(reset) timer1 //or timer0 ?????????
; 0002 0092         capture_edge=CCP_CAPTURE_FE; //change edge
; 0002 0093         setup_ccp1(CCP_CAPTURE_FE);
; 0002 0094 //        output_high(GRN_LED); //DEBUG
; 0002 0095     }
; 0002 0096     else if(capture_edge==CCP_CAPTURE_FE) //if previous Edge was Rising (hi level)
; 0002 0097     {
; 0002 0098         t1_val=get_timer1();
; 0002 0099         set_timer1(0x00); //null(reset) timer1 //or timer0 ?????????
; 0002 009A         capture_edge=CCP_CAPTURE_RE; //change edge
; 0002 009B         setup_ccp1(CCP_CAPTURE_RE);
; 0002 009C //        output_low(GRN_LED); //DEBUG
; 0002 009D     }
; 0002 009E */
; 0002 009F     t1_val=get_timer1();
	SBIW R28,7
	RCALL SUBOPT_0x9
	LDI  R30,LOW(0)
	STD  Y+5,R30
	STD  Y+6,R30
	RCALL __SAVELOCR6
;	t_t_val -> Y+9
;	t1_val -> R16,R17
;	t_1_2_bit -> R18,R19
;	t_1_2_delta -> R20,R21
;	rf_capt8_arr -> Y+6
	__GETWRN 18,19,0
	__GETWRN 20,21,0
	RCALL _get_timer1
	MOVW R16,R30
; 0002 00A0 //    t_t_val=(uint32_t)t1_val+(uint32_t)(0x0000FFFF*ovf_t1_cnt);
; 0002 00A1 //    t_t_val=ovf_t1_cnt;
; 0002 00A2     ovf_t1_cnt=0;
	RCALL SUBOPT_0xA
	STS  _ovf_t1_cnt,R30
	STS  _ovf_t1_cnt+1,R31
; 0002 00A3 
; 0002 00A4     set_timer1(0x00); //null(reset) timer1 //or timer0 ?????????
	RCALL SUBOPT_0xA
	RCALL SUBOPT_0x1
	RCALL _set_timer1
; 0002 00A5 
; 0002 00A6 //!!!!!!!!!!! now capture_edge is changed !!!!!!!!!!!
; 0002 00A7 /*
; 0002 00A8 
; 0002 00A9 //===============================ARTEFACT ERROR bits===============================
; 0002 00AA     if(t1_val>50 && t1_val<150 ) //catching by the low level artefacts
; 0002 00AB     {
; 0002 00AC //        t1_val=t_half_1_bit; //
; 0002 00AD         rf_capture_cnt--;
; 0002 00AE         capture_edge=CCP_CAPTURE_RE; //trick, to saving in t_half_1_bit
; 0002 00AF //        YEL_LED_TGL;
; 0002 00B0 //        set_timer1(t_half_1_bit-100); //add prev val
; 0002 00B1 //        set_timer1(200); //add prev val
; 0002 00B2     }
; 0002 00B3     if(t1_val>50 && t1_val<150 && capture_edge==CCP_CAPTURE_FE) //catching by the low level artefacts
; 0002 00B4     {
; 0002 00B5 //        output_high(YEL_LED);
; 0002 00B6     }
; 0002 00B7     else if(t1_val>50 && t1_val<150 && capture_edge==CCP_CAPTURE_RE) //catching by the high level artefacts
; 0002 00B8     {
; 0002 00B9 //        output_high(YEL_LED);
; 0002 00BA     }
; 0002 00BB */
; 0002 00BC //===============================ARTEFACT ERROR bits===============================
; 0002 00BD 
; 0002 00BE 
; 0002 00BF 
; 0002 00C0     if(t1_val>12000 && t1_val<20000 && RF_IN_PIN && !rf_capture_cnt) //start stop //catching the low level pulse
	__CPWRN 16,17,12001
	BRLO _0x40011
	__CPWRN 16,17,20000
	BRSH _0x40011
	SBIS 0x10,2
	RJMP _0x40011
	TST  R5
	BREQ _0x40012
_0x40011:
	RJMP _0x40010
_0x40012:
; 0002 00C1 //    if(t1_val>24000 && t1_val<40000 && RF_IN_PIN && !rf_capture_cnt) //start stop //catching the low level pulse
; 0002 00C2     {
; 0002 00C3         rf_capture_array[rf_capture_cnt]=0;
	MOV  R30,R5
	RCALL SUBOPT_0xB
	RCALL SUBOPT_0xC
; 0002 00C4         rf_capture_cnt=1; //this mean that rf recording is started
	LDI  R30,LOW(1)
	MOV  R5,R30
; 0002 00C5 //        YEL_LED_OFF;  //DEBUG
; 0002 00C6 //          YEL_LED_TGL;  //DEBUG
; 0002 00C7     }
; 0002 00C8     else if(rf_capture_cnt%2!=0 && !RF_IN_PIN && rf_capture_cnt) //catching the high level pulse
	RJMP _0x40013
_0x40010:
	RCALL SUBOPT_0xD
	BREQ _0x40015
	SBIC 0x10,2
	RJMP _0x40015
	TST  R5
	BRNE _0x40016
_0x40015:
	RJMP _0x40014
_0x40016:
; 0002 00C9     {
; 0002 00CA //        YEL_LED_ON;  //DEBUG
; 0002 00CB //===============================ARTEFACT ERROR bits===============================
; 0002 00CC         t_half_1_bit=t1_val; //save half bit time to compare with next half bit
	MOVW R8,R16
; 0002 00CD         rf_capture_cnt++;
	RJMP _0x4004C
; 0002 00CE     }
; 0002 00CF     else if(rf_capture_cnt%2==0 && RF_IN_PIN && rf_capture_cnt) //catching the low level pulse
_0x40014:
	RCALL SUBOPT_0xD
	BRNE _0x40019
	SBIS 0x10,2
	RJMP _0x40019
	TST  R5
	BRNE _0x4001A
_0x40019:
	RJMP _0x40018
_0x4001A:
; 0002 00D0     {
; 0002 00D1 //        YEL_LED_OFF; //DEBUG
; 0002 00D2         if(t1_val>50 && t1_val<150 ) //catching the low level artefacts
	__CPWRN 16,17,51
	BRLO _0x4001C
	__CPWRN 16,17,150
	BRLO _0x4001D
_0x4001C:
	RJMP _0x4001B
_0x4001D:
; 0002 00D3 //        if(t1_val>100 && t1_val<300 ) //catching the low level artefacts
; 0002 00D4         {
; 0002 00D5     //        t1_val=t_half_1_bit; //
; 0002 00D6             rf_capture_cnt--;
	DEC  R5
; 0002 00D7     //        YEL_LED_TGL;  //DEBUG
; 0002 00D8             set_timer1(t_half_1_bit); //add prev val
	ST   -Y,R9
	ST   -Y,R8
	RCALL _set_timer1
; 0002 00D9     //        set_timer1(200); //add prev val
; 0002 00DA         }
; 0002 00DB         else
	RJMP _0x4001E
_0x4001B:
; 0002 00DC         {
; 0002 00DD             t_half_2_bit=t1_val;
	MOVW R10,R16
; 0002 00DE             t_1_2_bit=((uint32_t)t_half_1_bit+(uint32_t)t_half_2_bit)>>2; //add 1/4 and 3/4 of sigal and divide by 4
	MOVW R30,R8
	RCALL SUBOPT_0xE
	MOVW R26,R30
	MOVW R24,R22
	MOVW R30,R10
	RCALL SUBOPT_0xE
	RCALL __ADDD21
	LDI  R30,LOW(2)
	RCALL __LSRD12
	MOVW R18,R30
; 0002 00DF             if(t_half_1_bit<t_half_2_bit)
	__CPWRR 8,9,10,11
	BRSH _0x4001F
; 0002 00E0             {
; 0002 00E1                 t_1_2_delta=t_half_1_bit/delta_bit; //calculate delta deviations from one half bit time to another
	MOVW R30,R12
	MOVW R26,R8
	RCALL __DIVW21U
	MOVW R20,R30
; 0002 00E2                 if(t_1_2_bit>(t_half_1_bit-t_1_2_delta) && t_1_2_bit<(t_half_1_bit+t_1_2_delta))
	MOVW R30,R8
	RCALL SUBOPT_0xF
	BRSH _0x40021
	MOVW R30,R20
	ADD  R30,R8
	ADC  R31,R9
	CP   R18,R30
	CPC  R19,R31
	BRLO _0x40022
_0x40021:
	RJMP _0x40020
_0x40022:
; 0002 00E3                 {
; 0002 00E4                     rf_capture_array[rf_capture_cnt/2]=0; //save bit
	RCALL SUBOPT_0x10
	RCALL SUBOPT_0xC
; 0002 00E5                 }
; 0002 00E6                 else
	RJMP _0x40023
_0x40020:
; 0002 00E7                 {
; 0002 00E8                     rf_capture_cnt=0; //reset counter for saving bits and start again
	CLR  R5
; 0002 00E9                 }
_0x40023:
; 0002 00EA             }
; 0002 00EB             else if(t_half_1_bit>t_half_2_bit)
	RJMP _0x40024
_0x4001F:
	__CPWRR 10,11,8,9
	BRSH _0x40025
; 0002 00EC             {
; 0002 00ED                 t_1_2_delta=t_half_2_bit/delta_bit;
	MOVW R30,R12
	MOVW R26,R10
	RCALL __DIVW21U
	MOVW R20,R30
; 0002 00EE                 if(t_1_2_bit>(t_half_2_bit-t_1_2_delta) && t_1_2_bit<(t_half_2_bit+t_1_2_delta))
	MOVW R30,R10
	RCALL SUBOPT_0xF
	BRSH _0x40027
	MOVW R30,R20
	ADD  R30,R10
	ADC  R31,R11
	CP   R18,R30
	CPC  R19,R31
	BRLO _0x40028
_0x40027:
	RJMP _0x40026
_0x40028:
; 0002 00EF                 {
; 0002 00F0                     rf_capture_array[rf_capture_cnt/2]=1; //save bit
	RCALL SUBOPT_0x10
	RCALL SUBOPT_0x11
	LDI  R26,LOW(1)
	STD  Z+0,R26
; 0002 00F1                 }
; 0002 00F2                 else
	RJMP _0x40029
_0x40026:
; 0002 00F3                 {
; 0002 00F4                     rf_capture_cnt=0; //reset counter for saving bits and start again
	CLR  R5
; 0002 00F5                 }
_0x40029:
; 0002 00F6             }
; 0002 00F7             else // if(t_half_1_bit==t_half_2_bit) //may happends, who know
	RJMP _0x4002A
_0x40025:
; 0002 00F8             {
; 0002 00F9                 rf_capture_cnt=0;
	CLR  R5
; 0002 00FA             }
_0x4002A:
_0x40024:
; 0002 00FB 
; 0002 00FC             if(rf_capture_cnt) //else if nulled - do not increment
	TST  R5
	BREQ _0x4002B
; 0002 00FD             {
; 0002 00FE                 rf_capture_cnt++;
_0x4004C:
	INC  R5
; 0002 00FF             }
; 0002 0100         }
_0x4002B:
_0x4001E:
; 0002 0101     }
; 0002 0102 
; 0002 0103 
; 0002 0104 //==================catching repeats==================
; 0002 0105     if(rf_capture_cnt)
_0x40018:
_0x40013:
	TST  R5
	BRNE PC+2
	RJMP _0x4002C
; 0002 0106     {
; 0002 0107 //        YEL_LED_TGL;  //DEBUG
; 0002 0108         if(rf_capture_array_prev[(rf_capture_cnt-1)>>1]==rf_capture_array[(rf_capture_cnt-1)>>1])
	RCALL SUBOPT_0x12
	SBIW R26,1
	RCALL SUBOPT_0x7
	MOVW R26,R30
	SUBI R30,LOW(-_rf_capture_array_prev)
	SBCI R31,HIGH(-_rf_capture_array_prev)
	LD   R0,Z
	MOVW R30,R26
	RCALL SUBOPT_0x11
	LD   R30,Z
	CP   R30,R0
	BREQ PC+2
	RJMP _0x4002D
; 0002 0109 //        if(rf_capture_array_prev[(rf_capture_cnt-1)/2]==rf_capture_array[(rf_capture_cnt-1)/2])
; 0002 010A         {
; 0002 010B 
; 0002 010C             if(rf_repeat_cnt==SHORT_RF_PRESS) //sure pressing on keychain button
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	CP   R30,R6
	CPC  R31,R7
	BRNE _0x4002E
; 0002 010D             {
; 0002 010E //                EMPTY_NODE; //???????????????? but work
; 0002 010F                 rf_disable(); //for compare_key operations
	RCALL _rf_disable
; 0002 0110 
; 0002 0111 /*
; 0002 0112                 rf_capture_convert(rf_capt8_arr);
; 0002 0113                 switch(compare_key_with_master(rf_capt8_arr))
; 0002 0114                 {
; 0002 0115                     case master_button_A:
; 0002 0116                     {
; 0002 0117                         keychain_short_status|=UNLOCK_KEYCHAIN_SHORT; // SELECT THIS WHEN BUTTON A HAVE SIMILAR FUNCTION
; 0002 0118 //                        keychain_status|=keychain_short_status;
; 0002 0119 //                        keychain_status|=UNLOCK_KEYCHAIN_SHORT; // SELECT THIS WHEN BUTTON A HAVE SIMILAR FUNCTION
; 0002 011A //                        keychain_status|=MASTER_BUTTON_A_SHORT;
; 0002 011B                     }
; 0002 011C                     break;
; 0002 011D                     case master_button_B:
; 0002 011E                     {
; 0002 011F                         keychain_short_status|=MASTER_BUTTON_B_SHORT;
; 0002 0120 //                        keychain_status|=MASTER_BUTTON_B_SHORT;
; 0002 0121                     }
; 0002 0122                     break;
; 0002 0123                     case master_button_C:
; 0002 0124                     {
; 0002 0125                         keychain_short_status|=MASTER_BUTTON_C_SHORT;
; 0002 0126 //                        keychain_status|=MASTER_BUTTON_C_SHORT;
; 0002 0127                     }
; 0002 0128                     break;
; 0002 0129                     case master_button_D:
; 0002 012A                     {
; 0002 012B                         keychain_short_status|=MASTER_BUTTON_D_SHORT;
; 0002 012C //                        keychain_status|=MASTER_BUTTON_D_SHORT;
; 0002 012D                     }
; 0002 012E                     break;
; 0002 012F                     case master_button_other:
; 0002 0130                     {
; 0002 0131                         //NO ACTION
; 0002 0132                     }
; 0002 0133                     break;
; 0002 0134                     default:
; 0002 0135                     {
; 0002 0136                         if(compare_key_with_ee(rf_capt8_arr))
; 0002 0137                         {
; 0002 0138                             keychain_short_status|=UNLOCK_KEYCHAIN_SHORT;
; 0002 0139                             keychain_status|=keychain_short_status;
; 0002 013A //                            keychain_status|=UNLOCK_KEYCHAIN_SHORT;
; 0002 013B                         }
; 0002 013C                     }
; 0002 013D                     break;
; 0002 013E                 }
; 0002 013F */
; 0002 0140 //                rf_repeat_cnt++;
; 0002 0141                 rf_enable();
	RCALL _rf_enable
; 0002 0142             }
; 0002 0143 //            else if(rf_repeat_cnt==LONG_RF_PRESS && !(save_keychains_status()&SAVE_K_EE_FL))
; 0002 0144             else if(rf_repeat_cnt==LONG_RF_PRESS)
	RJMP _0x4002F
_0x4002E:
	LDI  R30,LOW(3900)
	LDI  R31,HIGH(3900)
	CP   R30,R6
	CPC  R31,R7
	BRNE _0x40030
; 0002 0145             {
; 0002 0146 //                EMPTY_NODE; //???????????????? but work
; 0002 0147 //                disable_interrupts(INT_CCP1);
; 0002 0148 //            keychain_status|=(ANY_KEYCHAIN_BUTTON_LEAVED|keychain_short_status);
; 0002 0149                 keychain_status=keychain_short_status;
	RCALL SUBOPT_0x13
	RCALL SUBOPT_0x14
; 0002 014A                 if(keychain_status&MASTER_BUTTON_A_SHORT)
	LDS  R30,_keychain_status
	ANDI R30,LOW(0x4)
	BREQ _0x40031
; 0002 014B                 {
; 0002 014C                     keychain_status|=MASTER_BUTTON_A_LONG;
	RCALL SUBOPT_0x15
	ORI  R30,8
	RJMP _0x4004D
; 0002 014D                 }
; 0002 014E                 else if(keychain_status&MASTER_BUTTON_B_SHORT)
_0x40031:
	LDS  R30,_keychain_status
	ANDI R30,LOW(0x10)
	BREQ _0x40033
; 0002 014F                 {
; 0002 0150                     keychain_status|=MASTER_BUTTON_B_LONG;
	RCALL SUBOPT_0x15
	ORI  R30,0x20
	RJMP _0x4004D
; 0002 0151                 }
; 0002 0152                 else if(keychain_status&MASTER_BUTTON_C_SHORT)
_0x40033:
	LDS  R30,_keychain_status
	ANDI R30,LOW(0x40)
	BREQ _0x40035
; 0002 0153                 {
; 0002 0154                     keychain_status|=MASTER_BUTTON_C_LONG;
	RCALL SUBOPT_0x15
	ORI  R30,0x80
	RJMP _0x4004D
; 0002 0155                  }
; 0002 0156                 else if(keychain_status&MASTER_BUTTON_D_SHORT)
_0x40035:
	__GETB1MN _keychain_status,1
	ANDI R30,LOW(0x1)
	BREQ _0x40037
; 0002 0157                 {
; 0002 0158                     keychain_status|=MASTER_BUTTON_D_LONG;
	__ORBMNN _keychain_status,1,2
; 0002 0159                 }
; 0002 015A                 else if(keychain_status&UNLOCK_KEYCHAIN_SHORT)
	RJMP _0x40038
_0x40037:
	RCALL SUBOPT_0x15
	ANDI R30,LOW(0x1)
	BREQ _0x40039
; 0002 015B                 {
; 0002 015C                     keychain_status|=UNLOCK_KEYCHAIN_LONG;
	RCALL SUBOPT_0x15
	ORI  R30,2
_0x4004D:
	STS  _keychain_status,R30
	STS  _keychain_status+1,R31
; 0002 015D                 }
; 0002 015E 
; 0002 015F //                rf_repeat_cnt++;
; 0002 0160 //                enable_interrupts(INT_CCP1);
; 0002 0161             }
_0x40039:
_0x40038:
; 0002 0162 
; 0002 0163             if(rf_repeat_cnt<65500)
_0x40030:
_0x4002F:
	LDI  R30,LOW(65500)
	LDI  R31,HIGH(65500)
	CP   R6,R30
	CPC  R7,R31
	BRSH _0x4003A
; 0002 0164             {
; 0002 0165                 rf_repeat_cnt++;
	MOVW R30,R6
	ADIW R30,1
	MOVW R6,R30
; 0002 0166             }
; 0002 0167         }
_0x4003A:
; 0002 0168         else //if not repeat
	RJMP _0x4003B
_0x4002D:
; 0002 0169         {
; 0002 016A             rf_repeat_cnt=0;
	CLR  R6
	CLR  R7
; 0002 016B         }
_0x4003B:
; 0002 016C     }
; 0002 016D     else //if rf_capture_cnt==0
	RJMP _0x4003C
_0x4002C:
; 0002 016E     {
; 0002 016F //        if(keychain_status)
; 0002 0170         if(keychain_short_status)
	RCALL SUBOPT_0x13
	SBIW R30,0
	BREQ _0x4003D
; 0002 0171         {
; 0002 0172             keychain_status|=(ANY_KEYCHAIN_BUTTON_LEAVED|keychain_short_status);
	RCALL SUBOPT_0x13
	ORI  R31,HIGH(0x8000)
	LDS  R26,_keychain_status
	LDS  R27,_keychain_status+1
	OR   R30,R26
	OR   R31,R27
	RCALL SUBOPT_0x14
; 0002 0173         }
; 0002 0174         rf_repeat_cnt=0;
_0x4003D:
	CLR  R6
	CLR  R7
; 0002 0175     }
_0x4003C:
; 0002 0176 
; 0002 0177 //==================catching repeats==================
; 0002 0178 
; 0002 0179 //if arrived all key package
; 0002 017A     if((rf_capture_cnt>>1)>=(KEY_BITS-1)) //do not touch
	RCALL SUBOPT_0x12
	RCALL SUBOPT_0x7
	SBIW R30,48
	BRGE PC+2
	RJMP _0x4003E
; 0002 017B     {
; 0002 017C         uint8_t cp=0;
; 0002 017D         uint16_t tmpr1t=0;
; 0002 017E         int16_t  tmpr2t=0;
; 0002 017F         rf_disable();
	SBIW R28,5
	LDI  R30,LOW(0)
	ST   Y,R30
	STD  Y+1,R30
	STD  Y+2,R30
	RCALL SUBOPT_0x9
;	t_t_val -> Y+14
;	rf_capt8_arr -> Y+11
;	cp -> Y+4
;	tmpr1t -> Y+2
;	tmpr2t -> Y+0
	RCALL _rf_disable
; 0002 0180         rf_capture_cnt=0;
	CLR  R5
; 0002 0181 
; 0002 0182         YEL_LED_ON;  //DEBUG
	SBI  0x18,0
; 0002 0183 
; 0002 0184 //                printf("\n\rCode:\n\r");
; 0002 0185 //        printf("\nOVF: %03d %d\n",t_t_val, t1_val);
; 0002 0186 //        printf("\n");
; 0002 0187         for(prnt_cnt=0; prnt_cnt<KEY_BITS; prnt_cnt++)
	RCALL SUBOPT_0xA
	STS  _prnt_cnt,R30
	STS  _prnt_cnt+1,R31
_0x40042:
	LDS  R26,_prnt_cnt
	LDS  R27,_prnt_cnt+1
	SBIW R26,49
	BRSH _0x40043
; 0002 0188         {
; 0002 0189             if((prnt_cnt/8)<6) //convertion bit to byte
	RCALL SUBOPT_0x16
	RCALL __LSRW3
	SBIW R30,6
	BRSH _0x40044
; 0002 018A             {
; 0002 018B                 if(prnt_cnt%8==0)bit_8_capt_arr[prnt_cnt/8]=0;
	RCALL SUBOPT_0x16
	ANDI R30,LOW(0x7)
	BRNE _0x40045
	RCALL SUBOPT_0x17
	LDI  R26,LOW(0)
	STD  Z+0,R26
; 0002 018C                 bit_8_capt_arr[prnt_cnt/8]|=(rf_capture_array[prnt_cnt])<<(prnt_cnt%8);
_0x40045:
	RCALL SUBOPT_0x17
	MOVW R24,R30
	LD   R22,Z
	CLR  R23
	RCALL SUBOPT_0x16
	RCALL SUBOPT_0x11
	LD   R30,Z
	RCALL SUBOPT_0xB
	MOVW R26,R30
	RCALL SUBOPT_0x16
	ANDI R30,LOW(0x7)
	ANDI R31,HIGH(0x7)
	RCALL __LSLW12
	OR   R30,R22
	OR   R31,R23
	MOVW R26,R24
	ST   X,R30
; 0002 018D             }
; 0002 018E 
; 0002 018F //            if(prnt_cnt%8==0)printf("  ");
; 0002 0190 //            printf("%d ",rf_capture_array[prnt_cnt]);
; 0002 0191         }
_0x40044:
	LDI  R26,LOW(_prnt_cnt)
	LDI  R27,HIGH(_prnt_cnt)
	RCALL SUBOPT_0x8
	RJMP _0x40042
_0x40043:
; 0002 0192 
; 0002 0193 //        printf("\n");
; 0002 0194 //        for(prnt_cnt=0; prnt_cnt<6; prnt_cnt++)
; 0002 0195 //        {
; 0002 0196 //            printf("0x%02X ",bit_8_capt_arr[prnt_cnt]);
; 0002 0197 //        }
; 0002 0198 
; 0002 0199         if(CRC8(bit_8_capt_arr, 5)==bit_8_capt_arr[5])
	LDI  R30,LOW(_bit_8_capt_arr)
	LDI  R31,HIGH(_bit_8_capt_arr)
	RCALL SUBOPT_0x1
	LDI  R30,LOW(5)
	ST   -Y,R30
	RCALL _CRC8
	MOV  R26,R30
	__GETB1MN _bit_8_capt_arr,5
	CP   R30,R26
	BREQ PC+2
	RJMP _0x40046
; 0002 019A         {
; 0002 019B             printf("\n");
	__POINTW1FN _0x40000,0
	RCALL SUBOPT_0x1
	RCALL SUBOPT_0x2
; 0002 019C             printf("ID:0x%04X ",((uint16_t)(bit_8_capt_arr[0]<<8)|bit_8_capt_arr[1]) );
	__POINTW1FN _0x40000,2
	RCALL SUBOPT_0x1
	LDS  R27,_bit_8_capt_arr
	LDI  R26,LOW(0)
	__GETB1MN _bit_8_capt_arr,1
	RCALL SUBOPT_0xB
	OR   R30,R26
	OR   R31,R27
	RCALL SUBOPT_0x18
; 0002 019D             printf("COMMAND:0x%02X ",(bit_8_capt_arr[2]) );
	__POINTW1FN _0x40000,13
	RCALL SUBOPT_0x1
	__GETB1MN _bit_8_capt_arr,2
	CLR  R31
	CLR  R22
	CLR  R23
	RCALL SUBOPT_0x19
; 0002 019E             tmpr1t=(uint16_t)((bit_8_capt_arr[3]<<8)|bit_8_capt_arr[4]);
	__GETBRMN 27,_bit_8_capt_arr,3
	LDI  R26,LOW(0)
	__GETB1MN _bit_8_capt_arr,4
	RCALL SUBOPT_0xB
	OR   R30,R26
	OR   R31,R27
	STD  Y+2,R30
	STD  Y+2+1,R31
; 0002 019F             tmpr2t=((int16_t)tmpr1t-1000)/10;
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	SUBI R26,LOW(1000)
	SBCI R27,HIGH(1000)
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	RCALL __DIVW21
	ST   Y,R30
	STD  Y+1,R31
; 0002 01A0             if(bit_8_capt_arr[2]==0x05)
	__GETB1MN _bit_8_capt_arr,2
	CPI  R30,LOW(0x5)
	BRNE _0x40047
; 0002 01A1             {
; 0002 01A2                 printf("tval=%04d ",tmpr1t);
	__POINTW1FN _0x40000,29
	RCALL SUBOPT_0x1A
; 0002 01A3                 printf("tdeg=%03d",tmpr2t);
	__POINTW1FN _0x40000,40
	RCALL SUBOPT_0x1
	LDD  R30,Y+2
	LDD  R31,Y+2+1
	RCALL __CWD1
	RCALL SUBOPT_0x19
; 0002 01A4             }
; 0002 01A5             if(bit_8_capt_arr[2]==0x0B)
_0x40047:
	__GETB1MN _bit_8_capt_arr,2
	CPI  R30,LOW(0xB)
	BRNE _0x40048
; 0002 01A6             {
; 0002 01A7                 printf("uval=%04d ",tmpr1t);
	__POINTW1FN _0x40000,50
	RCALL SUBOPT_0x1A
; 0002 01A8                 printf("uvolt=%03d",tmpr1t*11/10240);
	__POINTW1FN _0x40000,61
	RCALL SUBOPT_0x1
	RCALL SUBOPT_0x4
	LDI  R30,LOW(11)
	RCALL __MULB1W2U
	MOVW R26,R30
	LDI  R30,LOW(10240)
	LDI  R31,HIGH(10240)
	RCALL __DIVW21U
	RCALL SUBOPT_0x18
; 0002 01A9             }
; 0002 01AA         }
_0x40048:
; 0002 01AB 
; 0002 01AC 
; 0002 01AD 
; 0002 01AE         for(cp=0;cp<KEY_BITS;cp++)
_0x40046:
	LDI  R30,LOW(0)
	STD  Y+4,R30
_0x4004A:
	LDD  R26,Y+4
	CPI  R26,LOW(0x31)
	BRSH _0x4004B
; 0002 01AF         {
; 0002 01B0             rf_capture_array_prev[cp]=rf_capture_array[cp];
	RCALL SUBOPT_0x1B
	SUBI R26,LOW(-_rf_capture_array_prev)
	SBCI R27,HIGH(-_rf_capture_array_prev)
	LDD  R30,Y+4
	RCALL SUBOPT_0xB
	RCALL SUBOPT_0x11
	LD   R30,Z
	ST   X,R30
; 0002 01B1         }
	LDD  R30,Y+4
	SUBI R30,-LOW(1)
	STD  Y+4,R30
	RJMP _0x4004A
_0x4004B:
; 0002 01B2         rf_enable();
	RCALL _rf_enable
; 0002 01B3     }
	ADIW R28,5
; 0002 01B4 }
_0x4003E:
	RCALL __LOADLOCR6
	ADIW R28,13
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	LD   R27,Y+
	LD   R26,Y+
	LD   R25,Y+
	LD   R24,Y+
	LD   R23,Y+
	LD   R22,Y+
	LD   R15,Y+
	LD   R1,Y+
	LD   R0,Y+
	RETI
;
;/*
;void openkey_switch_routine(void)
;{
;    if(!END_SWITCH) //if KEY_SWITCH is pressed
;    {
;//        YEL_LED_TGL;
;        if(openkey_sw_cnt>=KEY_SW_MAX)
;        {
;            //openkey_sw_on_cnt=KEY_SW_MAX;
;        }
;        else if(openkey_sw_cnt==KEY_SW_TRSHLD)
;        {
;            keychain_status=UNLOCK_KEYCHAIN_SHORT; //record bit
;            openkey_sw_cnt++;
;        }
;        else if(openkey_sw_cnt==50)
;        {
;            keychain_status|=UNLOCK_KEYCHAIN_LONG; //record bit
;            openkey_sw_cnt++;
;        }
;        else
;        {
;            openkey_sw_cnt++;
;        }
;    }
;    else //if(!OPENKEY_SWITCH)
;    {
;        openkey_sw_cnt=0;
;        keychain_status|=ANY_KEYCHAIN_BUTTON_LEAVED;
;
;    }
;}
;*/
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif

	.CSEG
_putchar:
     sbis usr,udre
     rjmp _putchar
     ld   r30,y
     out  udr,r30
	ADIW R28,1
	RET
__put_G100:
	RCALL __SAVELOCR2
	RCALL SUBOPT_0x4
	RCALL __GETW1P
	SBIW R30,0
	BREQ _0x2000010
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	RCALL __GETW1P
	MOVW R16,R30
	SBIW R30,0
	BREQ _0x2000012
	__CPWRN 16,17,2
	BRLO _0x2000013
	MOVW R30,R16
	SBIW R30,1
	MOVW R16,R30
	ST   X+,R30
	ST   X,R31
_0x2000012:
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0x8
	SBIW R30,1
	LDD  R26,Y+6
	STD  Z+0,R26
_0x2000013:
	RJMP _0x2000014
_0x2000010:
	LDD  R30,Y+6
	ST   -Y,R30
	RCALL _putchar
_0x2000014:
	RCALL __LOADLOCR2
	ADIW R28,7
	RET
__print_G100:
	SBIW R28,6
	RCALL __SAVELOCR6
	LDI  R17,0
_0x2000015:
	LDD  R30,Y+18
	LDD  R31,Y+18+1
	ADIW R30,1
	STD  Y+18,R30
	STD  Y+18+1,R31
	SBIW R30,1
	LPM  R30,Z
	MOV  R18,R30
	CPI  R30,0
	BRNE PC+2
	RJMP _0x2000017
	MOV  R30,R17
	RCALL SUBOPT_0xB
	SBIW R30,0
	BRNE _0x200001B
	CPI  R18,37
	BRNE _0x200001C
	LDI  R17,LOW(1)
	RJMP _0x200001D
_0x200001C:
	RCALL SUBOPT_0x1C
_0x200001D:
	RJMP _0x200001A
_0x200001B:
	CPI  R30,LOW(0x1)
	LDI  R26,HIGH(0x1)
	CPC  R31,R26
	BRNE _0x200001E
	CPI  R18,37
	BRNE _0x200001F
	RCALL SUBOPT_0x1C
	RJMP _0x20000BC
_0x200001F:
	LDI  R17,LOW(2)
	LDI  R20,LOW(0)
	LDI  R16,LOW(0)
	CPI  R18,45
	BRNE _0x2000020
	LDI  R16,LOW(1)
	RJMP _0x200001A
_0x2000020:
	CPI  R18,43
	BRNE _0x2000021
	LDI  R20,LOW(43)
	RJMP _0x200001A
_0x2000021:
	CPI  R18,32
	BRNE _0x2000022
	LDI  R20,LOW(32)
	RJMP _0x200001A
_0x2000022:
	RJMP _0x2000023
_0x200001E:
	CPI  R30,LOW(0x2)
	LDI  R26,HIGH(0x2)
	CPC  R31,R26
	BRNE _0x2000024
_0x2000023:
	LDI  R21,LOW(0)
	LDI  R17,LOW(3)
	CPI  R18,48
	BRNE _0x2000025
	RCALL SUBOPT_0x1D
	LDI  R30,LOW(128)
	LDI  R31,HIGH(128)
	RCALL SUBOPT_0x1E
	RJMP _0x200001A
_0x2000025:
	RJMP _0x2000026
_0x2000024:
	CPI  R30,LOW(0x3)
	LDI  R26,HIGH(0x3)
	CPC  R31,R26
	BREQ PC+2
	RJMP _0x200001A
_0x2000026:
	CPI  R18,48
	BRLO _0x2000029
	CPI  R18,58
	BRLO _0x200002A
_0x2000029:
	RJMP _0x2000028
_0x200002A:
	MOV  R26,R21
	RCALL SUBOPT_0x1B
	LDI  R30,LOW(10)
	LDI  R31,HIGH(10)
	MULS R30,R26
	MOVW R30,R0
	MOV  R21,R30
	MOV  R22,R21
	CLR  R23
	MOV  R26,R18
	RCALL SUBOPT_0x1B
	LDI  R30,LOW(48)
	LDI  R31,HIGH(48)
	RCALL __SWAPW12
	SUB  R30,R26
	SBC  R31,R27
	MOVW R26,R22
	ADD  R30,R26
	MOV  R21,R30
	RJMP _0x200001A
_0x2000028:
	MOV  R30,R18
	RCALL SUBOPT_0xB
	CPI  R30,LOW(0x63)
	LDI  R26,HIGH(0x63)
	CPC  R31,R26
	BRNE _0x200002E
	RCALL SUBOPT_0x1F
	RCALL SUBOPT_0x20
	RCALL SUBOPT_0x1F
	LDD  R26,Z+4
	ST   -Y,R26
	RCALL SUBOPT_0x21
	RJMP _0x200002F
_0x200002E:
	CPI  R30,LOW(0x73)
	LDI  R26,HIGH(0x73)
	CPC  R31,R26
	BRNE _0x2000031
	RCALL SUBOPT_0x22
	RCALL SUBOPT_0x23
	RCALL _strlen
	MOV  R17,R30
	RJMP _0x2000032
_0x2000031:
	CPI  R30,LOW(0x70)
	LDI  R26,HIGH(0x70)
	CPC  R31,R26
	BRNE _0x2000034
	RCALL SUBOPT_0x22
	RCALL SUBOPT_0x23
	RCALL _strlenf
	MOV  R17,R30
	RCALL SUBOPT_0x1D
	LDI  R30,LOW(8)
	LDI  R31,HIGH(8)
	RCALL SUBOPT_0x1E
_0x2000032:
	RCALL SUBOPT_0x1D
	LDI  R30,LOW(2)
	LDI  R31,HIGH(2)
	RCALL SUBOPT_0x1E
	RCALL SUBOPT_0x1D
	LDI  R30,LOW(65407)
	LDI  R31,HIGH(65407)
	RCALL SUBOPT_0x24
	LDI  R19,LOW(0)
	RJMP _0x2000035
_0x2000034:
	CPI  R30,LOW(0x64)
	LDI  R26,HIGH(0x64)
	CPC  R31,R26
	BREQ _0x2000038
	CPI  R30,LOW(0x69)
	LDI  R26,HIGH(0x69)
	CPC  R31,R26
	BRNE _0x2000039
_0x2000038:
	RCALL SUBOPT_0x1D
	RCALL SUBOPT_0x25
	RCALL SUBOPT_0x1E
	RJMP _0x200003A
_0x2000039:
	CPI  R30,LOW(0x75)
	LDI  R26,HIGH(0x75)
	CPC  R31,R26
	BRNE _0x200003B
_0x200003A:
	LDI  R30,LOW(_tbl10_G100*2)
	LDI  R31,HIGH(_tbl10_G100*2)
	RCALL SUBOPT_0x26
	LDI  R17,LOW(5)
	RJMP _0x200003C
_0x200003B:
	CPI  R30,LOW(0x58)
	LDI  R26,HIGH(0x58)
	CPC  R31,R26
	BRNE _0x200003E
	RCALL SUBOPT_0x1D
	LDI  R30,LOW(8)
	LDI  R31,HIGH(8)
	RCALL SUBOPT_0x1E
	RJMP _0x200003F
_0x200003E:
	CPI  R30,LOW(0x78)
	LDI  R26,HIGH(0x78)
	CPC  R31,R26
	BREQ PC+2
	RJMP _0x2000070
_0x200003F:
	LDI  R30,LOW(_tbl16_G100*2)
	LDI  R31,HIGH(_tbl16_G100*2)
	RCALL SUBOPT_0x26
	LDI  R17,LOW(4)
_0x200003C:
	RCALL SUBOPT_0x1D
	RCALL SUBOPT_0x27
	BREQ _0x2000041
	RCALL SUBOPT_0x22
	RCALL SUBOPT_0x28
	LDD  R26,Y+11
	TST  R26
	BRPL _0x2000042
	LDD  R30,Y+10
	LDD  R31,Y+10+1
	RCALL __ANEGW1
	STD  Y+10,R30
	STD  Y+10+1,R31
	LDI  R20,LOW(45)
_0x2000042:
	CPI  R20,0
	BREQ _0x2000043
	SUBI R17,-LOW(1)
	RJMP _0x2000044
_0x2000043:
	RCALL SUBOPT_0x1D
	RCALL SUBOPT_0x29
_0x2000044:
	RJMP _0x2000045
_0x2000041:
	RCALL SUBOPT_0x22
	RCALL SUBOPT_0x28
_0x2000045:
_0x2000035:
	RCALL SUBOPT_0x2A
	BRNE _0x2000046
_0x2000047:
	CP   R17,R21
	BRSH _0x2000049
	RCALL SUBOPT_0x1D
	RCALL SUBOPT_0x2B
	BREQ _0x200004A
	RCALL SUBOPT_0x1D
	RCALL SUBOPT_0x27
	BREQ _0x200004B
	RCALL SUBOPT_0x1D
	RCALL SUBOPT_0x29
	MOV  R18,R20
	SUBI R17,LOW(1)
	RJMP _0x200004C
_0x200004B:
	LDI  R18,LOW(48)
_0x200004C:
	RJMP _0x200004D
_0x200004A:
	LDI  R18,LOW(32)
_0x200004D:
	RCALL SUBOPT_0x1C
	SUBI R21,LOW(1)
	RJMP _0x2000047
_0x2000049:
_0x2000046:
	MOV  R19,R17
	RCALL SUBOPT_0x1D
	LDI  R30,LOW(2)
	LDI  R31,HIGH(2)
	RCALL SUBOPT_0x2C
	BREQ _0x200004E
_0x200004F:
	CPI  R19,0
	BREQ _0x2000051
	RCALL SUBOPT_0x1D
	LDI  R30,LOW(8)
	LDI  R31,HIGH(8)
	RCALL SUBOPT_0x2C
	BREQ _0x2000052
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	ADIW R30,1
	RCALL SUBOPT_0x26
	SBIW R30,1
	LPM  R30,Z
	RJMP _0x20000BD
_0x2000052:
	LDD  R26,Y+6
	LDD  R27,Y+6+1
	LD   R30,X+
	STD  Y+6,R26
	STD  Y+6+1,R27
_0x20000BD:
	ST   -Y,R30
	RCALL SUBOPT_0x21
	CPI  R21,0
	BREQ _0x2000054
	SUBI R21,LOW(1)
_0x2000054:
	SUBI R19,LOW(1)
	RJMP _0x200004F
_0x2000051:
	RJMP _0x2000055
_0x200004E:
_0x2000057:
	LDI  R18,LOW(48)
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	RCALL __GETW1PF
	STD  Y+8,R30
	STD  Y+8+1,R31
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	ADIW R30,2
	RCALL SUBOPT_0x26
_0x2000059:
	LDD  R30,Y+8
	LDD  R31,Y+8+1
	LDD  R26,Y+10
	LDD  R27,Y+10+1
	CP   R26,R30
	CPC  R27,R31
	BRLO _0x200005B
	SUBI R18,-LOW(1)
	LDD  R26,Y+8
	LDD  R27,Y+8+1
	LDD  R30,Y+10
	LDD  R31,Y+10+1
	SUB  R30,R26
	SBC  R31,R27
	STD  Y+10,R30
	STD  Y+10+1,R31
	RJMP _0x2000059
_0x200005B:
	CPI  R18,58
	BRLO _0x200005C
	RCALL SUBOPT_0x1D
	LDI  R30,LOW(8)
	LDI  R31,HIGH(8)
	RCALL SUBOPT_0x2C
	BREQ _0x200005D
	MOV  R30,R18
	RCALL SUBOPT_0xB
	ADIW R30,7
	RJMP _0x20000BE
_0x200005D:
	MOV  R30,R18
	RCALL SUBOPT_0xB
	ADIW R30,39
_0x20000BE:
	MOV  R18,R30
_0x200005C:
	RCALL SUBOPT_0x1D
	LDI  R30,LOW(16)
	LDI  R31,HIGH(16)
	RCALL SUBOPT_0x2C
	BRNE _0x2000060
	CPI  R18,49
	BRSH _0x2000062
	LDD  R26,Y+8
	LDD  R27,Y+8+1
	SBIW R26,1
	BRNE _0x2000061
_0x2000062:
	RJMP _0x20000BF
_0x2000061:
	CP   R21,R19
	BRLO _0x2000066
	RCALL SUBOPT_0x2A
	BREQ _0x2000067
_0x2000066:
	RJMP _0x2000065
_0x2000067:
	LDI  R18,LOW(32)
	RCALL SUBOPT_0x1D
	RCALL SUBOPT_0x2B
	BREQ _0x2000068
	LDI  R18,LOW(48)
_0x20000BF:
	MOV  R26,R16
	RCALL SUBOPT_0x1B
	LDI  R30,LOW(16)
	LDI  R31,HIGH(16)
	RCALL SUBOPT_0x1E
	RCALL SUBOPT_0x1D
	RCALL SUBOPT_0x27
	BREQ _0x2000069
	RCALL SUBOPT_0x1D
	RCALL SUBOPT_0x29
	ST   -Y,R20
	RCALL SUBOPT_0x21
	CPI  R21,0
	BREQ _0x200006A
	SUBI R21,LOW(1)
_0x200006A:
_0x2000069:
_0x2000068:
_0x2000060:
	RCALL SUBOPT_0x1C
	CPI  R21,0
	BREQ _0x200006B
	SUBI R21,LOW(1)
_0x200006B:
_0x2000065:
	SUBI R19,LOW(1)
	LDD  R26,Y+8
	LDD  R27,Y+8+1
	SBIW R26,2
	BRLO _0x2000058
	RJMP _0x2000057
_0x2000058:
_0x2000055:
	RCALL SUBOPT_0x2A
	BREQ _0x200006C
_0x200006D:
	CPI  R21,0
	BREQ _0x200006F
	SUBI R21,LOW(1)
	LDI  R30,LOW(32)
	ST   -Y,R30
	RCALL SUBOPT_0x21
	RJMP _0x200006D
_0x200006F:
_0x200006C:
_0x2000070:
_0x200002F:
_0x20000BC:
	LDI  R17,LOW(0)
_0x200001A:
	RJMP _0x2000015
_0x2000017:
	RCALL __LOADLOCR6
	ADIW R28,20
	RET
_printf:
	PUSH R15
	MOV  R15,R24
	SBIW R28,2
	RCALL __SAVELOCR2
	MOVW R26,R28
	RCALL __ADDW2R15
	MOVW R16,R26
	LDI  R30,0
	STD  Y+2,R30
	STD  Y+2+1,R30
	MOVW R26,R28
	ADIW R26,4
	RCALL __ADDW2R15
	RCALL __GETW1P
	RCALL SUBOPT_0x1
	ST   -Y,R17
	ST   -Y,R16
	MOVW R30,R28
	ADIW R30,6
	RCALL SUBOPT_0x1
	RCALL SUBOPT_0xA
	RCALL SUBOPT_0x1
	RCALL __print_G100
	RCALL __LOADLOCR2
	ADIW R28,4
	POP  R15
	RET

	.CSEG

	.CSEG
_strlen:
    ld   r26,y+
    ld   r27,y+
    clr  r30
    clr  r31
strlen0:
    ld   r22,x+
    tst  r22
    breq strlen1
    adiw r30,1
    rjmp strlen0
strlen1:
    ret
_strlenf:
    clr  r26
    clr  r27
    ld   r30,y+
    ld   r31,y+
strlenf0:
    lpm  r0,z+
    tst  r0
    breq strlenf1
    adiw r26,1
    rjmp strlenf0
strlenf1:
    movw r30,r26
    ret

	.DSEG
_rf_capture_array:
	.BYTE 0x31
_rf_capture_array_prev:
	.BYTE 0x31
_bit_8_capt_arr:
	.BYTE 0x6
_keychain_status:
	.BYTE 0x2
_keychain_short_status:
	.BYTE 0x2
_prnt_cnt:
	.BYTE 0x2
_ovf_t1_cnt:
	.BYTE 0x2
_p_S1020024:
	.BYTE 0x2

	.CSEG
;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x0:
	IN   R30,0x3B
	LDI  R31,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 35 TIMES, CODE SIZE REDUCTION:32 WORDS
SUBOPT_0x1:
	ST   -Y,R31
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x2:
	LDI  R24,0
	RCALL _printf
	ADIW R28,2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x3:
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
	RCALL SUBOPT_0x1
	RJMP _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x4:
	LDD  R26,Y+4
	LDD  R27,Y+4+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x5:
	LDD  R30,Y+2
	LDI  R31,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x6:
	MOV  R26,R17
	LDI  R27,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x7:
	MOVW R30,R26
	ASR  R31
	ROR  R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x8:
	LD   R30,X+
	LD   R31,X+
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x9:
	LDI  R30,LOW(0)
	STD  Y+3,R30
	STD  Y+4,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0xA:
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 12 TIMES, CODE SIZE REDUCTION:20 WORDS
SUBOPT_0xB:
	LDI  R31,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0xC:
	SUBI R30,LOW(-_rf_capture_array)
	SBCI R31,HIGH(-_rf_capture_array)
	LDI  R26,LOW(0)
	STD  Z+0,R26
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0xD:
	MOV  R26,R5
	CLR  R27
	LDI  R30,LOW(2)
	LDI  R31,HIGH(2)
	RCALL __MODW21
	SBIW R30,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0xE:
	CLR  R22
	CLR  R23
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0xF:
	SUB  R30,R20
	SBC  R31,R21
	CP   R30,R18
	CPC  R31,R19
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x10:
	MOV  R26,R5
	LDI  R27,0
	LDI  R30,LOW(2)
	LDI  R31,HIGH(2)
	RCALL __DIVW21
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x11:
	SUBI R30,LOW(-_rf_capture_array)
	SBCI R31,HIGH(-_rf_capture_array)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x12:
	MOV  R26,R5
	LDI  R27,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x13:
	LDS  R30,_keychain_short_status
	LDS  R31,_keychain_short_status+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x14:
	STS  _keychain_status,R30
	STS  _keychain_status+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x15:
	LDS  R30,_keychain_status
	LDS  R31,_keychain_status+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:13 WORDS
SUBOPT_0x16:
	LDS  R30,_prnt_cnt
	LDS  R31,_prnt_cnt+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x17:
	RCALL SUBOPT_0x16
	RCALL __LSRW3
	SUBI R30,LOW(-_bit_8_capt_arr)
	SBCI R31,HIGH(-_bit_8_capt_arr)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x18:
	RCALL SUBOPT_0xE
	RCALL __PUTPARD1
	LDI  R24,4
	RCALL _printf
	ADIW R28,6
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x19:
	RCALL __PUTPARD1
	LDI  R24,4
	RCALL _printf
	ADIW R28,6
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x1A:
	RCALL SUBOPT_0x1
	LDD  R30,Y+4
	LDD  R31,Y+4+1
	RJMP SUBOPT_0x18

;OPTIMIZER ADDED SUBROUTINE, CALLED 22 TIMES, CODE SIZE REDUCTION:40 WORDS
SUBOPT_0x1B:
	LDI  R27,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:19 WORDS
SUBOPT_0x1C:
	ST   -Y,R18
	LDD  R30,Y+15
	LDD  R31,Y+15+1
	RCALL SUBOPT_0x1
	MOVW R30,R28
	ADIW R30,15
	RCALL SUBOPT_0x1
	RJMP __put_G100

;OPTIMIZER ADDED SUBROUTINE, CALLED 18 TIMES, CODE SIZE REDUCTION:15 WORDS
SUBOPT_0x1D:
	MOV  R26,R16
	RJMP SUBOPT_0x1B

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x1E:
	OR   R30,R26
	MOV  R16,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x1F:
	LDD  R30,Y+16
	LDD  R31,Y+16+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x20:
	SBIW R30,4
	STD  Y+16,R30
	STD  Y+16+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:16 WORDS
SUBOPT_0x21:
	LDD  R30,Y+15
	LDD  R31,Y+15+1
	RCALL SUBOPT_0x1
	MOVW R30,R28
	ADIW R30,15
	RCALL SUBOPT_0x1
	RJMP __put_G100

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x22:
	RCALL SUBOPT_0x1F
	RJMP SUBOPT_0x20

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x23:
	LDD  R26,Y+16
	LDD  R27,Y+16+1
	ADIW R26,4
	RCALL __GETW1P
	STD  Y+6,R30
	STD  Y+6+1,R31
	RJMP SUBOPT_0x1

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x24:
	AND  R30,R26
	MOV  R16,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x25:
	LDI  R30,LOW(4)
	LDI  R31,HIGH(4)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x26:
	STD  Y+6,R30
	STD  Y+6+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x27:
	RCALL SUBOPT_0x25
	AND  R30,R26
	AND  R31,R27
	SBIW R30,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x28:
	LDD  R26,Y+16
	LDD  R27,Y+16+1
	ADIW R26,4
	RCALL __GETW1P
	STD  Y+10,R30
	STD  Y+10+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x29:
	LDI  R30,LOW(65531)
	LDI  R31,HIGH(65531)
	RJMP SUBOPT_0x24

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x2A:
	MOV  R30,R16
	RCALL SUBOPT_0xB
	ANDI R30,LOW(0x1)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x2B:
	LDI  R30,LOW(128)
	LDI  R31,HIGH(128)
	AND  R30,R26
	AND  R31,R27
	SBIW R30,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x2C:
	AND  R30,R26
	AND  R31,R27
	SBIW R30,0
	RET


	.CSEG
_delay_ms:
	ld   r30,y+
	ld   r31,y+
	adiw r30,0
	breq __delay_ms1
__delay_ms0:
	__DELAY_USW 0xFA0
	wdr
	sbiw r30,1
	brne __delay_ms0
__delay_ms1:
	ret

__ADDW2R15:
	CLR  R0
	ADD  R26,R15
	ADC  R27,R0
	RET

__ADDD21:
	ADD  R26,R30
	ADC  R27,R31
	ADC  R24,R22
	ADC  R25,R23
	RET

__ANEGW1:
	NEG  R31
	NEG  R30
	SBCI R31,0
	RET

__LSLW12:
	TST  R30
	MOV  R0,R30
	MOVW R30,R26
	BREQ __LSLW12R
__LSLW12L:
	LSL  R30
	ROL  R31
	DEC  R0
	BRNE __LSLW12L
__LSLW12R:
	RET

__LSRD12:
	TST  R30
	MOV  R0,R30
	MOVW R30,R26
	MOVW R22,R24
	BREQ __LSRD12R
__LSRD12L:
	LSR  R23
	ROR  R22
	ROR  R31
	ROR  R30
	DEC  R0
	BRNE __LSRD12L
__LSRD12R:
	RET

__LSRW3:
	LSR  R31
	ROR  R30
__LSRW2:
	LSR  R31
	ROR  R30
	LSR  R31
	ROR  R30
	RET

__CWD1:
	MOV  R22,R31
	ADD  R22,R22
	SBC  R22,R22
	MOV  R23,R22
	RET

__MULB1W2U:
	MOV  R22,R30
	MUL  R22,R26
	MOVW R30,R0
	MUL  R22,R27
	ADD  R31,R0
	RET

__DIVW21U:
	CLR  R0
	CLR  R1
	LDI  R25,16
__DIVW21U1:
	LSL  R26
	ROL  R27
	ROL  R0
	ROL  R1
	SUB  R0,R30
	SBC  R1,R31
	BRCC __DIVW21U2
	ADD  R0,R30
	ADC  R1,R31
	RJMP __DIVW21U3
__DIVW21U2:
	SBR  R26,1
__DIVW21U3:
	DEC  R25
	BRNE __DIVW21U1
	MOVW R30,R26
	MOVW R26,R0
	RET

__DIVW21:
	RCALL __CHKSIGNW
	RCALL __DIVW21U
	BRTC __DIVW211
	RCALL __ANEGW1
__DIVW211:
	RET

__MODW21:
	CLT
	SBRS R27,7
	RJMP __MODW211
	COM  R26
	COM  R27
	ADIW R26,1
	SET
__MODW211:
	SBRC R31,7
	RCALL __ANEGW1
	RCALL __DIVW21U
	MOVW R30,R26
	BRTC __MODW212
	RCALL __ANEGW1
__MODW212:
	RET

__CHKSIGNW:
	CLT
	SBRS R31,7
	RJMP __CHKSW1
	RCALL __ANEGW1
	SET
__CHKSW1:
	SBRS R27,7
	RJMP __CHKSW2
	COM  R26
	COM  R27
	ADIW R26,1
	BLD  R0,0
	INC  R0
	BST  R0,0
__CHKSW2:
	RET

__GETW1P:
	LD   R30,X+
	LD   R31,X
	SBIW R26,1
	RET

__GETW1PF:
	LPM  R0,Z+
	LPM  R31,Z
	MOV  R30,R0
	RET

__PUTPARD1:
	ST   -Y,R23
	ST   -Y,R22
	ST   -Y,R31
	ST   -Y,R30
	RET

__SWAPW12:
	MOV  R1,R27
	MOV  R27,R31
	MOV  R31,R1

__SWAPB12:
	MOV  R1,R26
	MOV  R26,R30
	MOV  R30,R1
	RET

__SAVELOCR6:
	ST   -Y,R21
__SAVELOCR5:
	ST   -Y,R20
__SAVELOCR4:
	ST   -Y,R19
__SAVELOCR3:
	ST   -Y,R18
__SAVELOCR2:
	ST   -Y,R17
	ST   -Y,R16
	RET

__LOADLOCR6:
	LDD  R21,Y+5
__LOADLOCR5:
	LDD  R20,Y+4
__LOADLOCR4:
	LDD  R19,Y+3
__LOADLOCR3:
	LDD  R18,Y+2
__LOADLOCR2:
	LDD  R17,Y+1
	LD   R16,Y
	RET

;END OF CODE MARKER
__END_OF_CODE:
