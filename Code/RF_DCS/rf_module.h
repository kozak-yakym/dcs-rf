#ifndef RF_MODULE_H
#define RF_MODULE_H

#include "c_types.h"

//#define KEY_BITS 26 //25bits+stop bit
#define KEY_BITS 49 //48bits+stop bit
#define REPEAT_NUM 150 //some action after repetition of code this number times
#define TRSHOLD_NUM 3 //some action after repetition of code this number times

#define SHORT_RF_PRESS  10 //78//(TRSHOLD_NUM*KEY_BITS)  //must be <65500
#define LONG_RF_PRESS   3900//450//(REPEAT_NUM*KEY_BITS)   //must be <65500


//keychain_status
#define UNLOCK_KEYCHAIN_SHORT           0b0000000000000001
#define UNLOCK_KEYCHAIN_LONG            0b0000000000000010
#define MASTER_BUTTON_A_SHORT           0b0000000000000100
#define MASTER_BUTTON_A_LONG            0b0000000000001000
#define MASTER_BUTTON_B_SHORT           0b0000000000010000
#define MASTER_BUTTON_B_LONG            0b0000000000100000
#define MASTER_BUTTON_C_SHORT           0b0000000001000000
#define MASTER_BUTTON_C_LONG            0b0000000010000000
#define MASTER_BUTTON_D_SHORT           0b0000000100000000
#define MASTER_BUTTON_D_LONG            0b0000001000000000
#define ANY_KEYCHAIN_BUTTON_LEAVED      0b1000000000000000

#define KEY_LENGTH              3 //in bytes


void rf_disable(void);
void rf_enable(void);
uint16_t pressed_keychain_status(void);
void pressed_keychain_status_clear(void);
void openkey_switch_routine(void);
//DON'T MOVE THIS FUNCTION because it don't work as void rf_data_convert(int1 *in_arr, unsigned int8 *out_arr)
void rf_capture_convert(uint8_t *out_arr);

#endif  //RF_MODULE_H