
#pragma used+
sfrb TWBR=0;
sfrb TWSR=1;
sfrb TWAR=2;
sfrb TWDR=3;
sfrb ADCL=4;
sfrb ADCH=5;
sfrw ADCW=4;      
sfrb ADCSRA=6;
sfrb ADMUX=7;
sfrb ACSR=8;
sfrb UBRRL=9;
sfrb UCSRB=0xa;
sfrb UCSRA=0xb;
sfrb UDR=0xc;
sfrb SPCR=0xd;
sfrb SPSR=0xe;
sfrb SPDR=0xf;
sfrb PIND=0x10;
sfrb DDRD=0x11;
sfrb PORTD=0x12;
sfrb PINC=0x13;
sfrb DDRC=0x14;
sfrb PORTC=0x15;
sfrb PINB=0x16;
sfrb DDRB=0x17;
sfrb PORTB=0x18;
sfrb EECR=0x1c;
sfrb EEDR=0x1d;
sfrb EEARL=0x1e;
sfrb EEARH=0x1f;
sfrw EEAR=0x1e;   
sfrb UBRRH=0x20;
sfrb UCSRC=0X20;
sfrb WDTCR=0x21;
sfrb ASSR=0x22;
sfrb OCR2=0x23;
sfrb TCNT2=0x24;
sfrb TCCR2=0x25;
sfrb ICR1L=0x26;
sfrb ICR1H=0x27;
sfrw ICR1=0x26;   
sfrb OCR1BL=0x28;
sfrb OCR1BH=0x29;
sfrw OCR1B=0x28;  
sfrb OCR1AL=0x2a;
sfrb OCR1AH=0x2b;
sfrw OCR1A=0x2a;  
sfrb TCNT1L=0x2c;
sfrb TCNT1H=0x2d;
sfrw TCNT1=0x2c;  
sfrb TCCR1B=0x2e;
sfrb TCCR1A=0x2f;
sfrb SFIOR=0x30;
sfrb OSCCAL=0x31;
sfrb TCNT0=0x32;
sfrb TCCR0=0x33;
sfrb MCUCSR=0x34;
sfrb MCUCR=0x35;
sfrb TWCR=0x36;
sfrb SPMCR=0x37;
sfrb TIFR=0x38;
sfrb TIMSK=0x39;
sfrb GIFR=0x3a;
sfrb GICR=0x3b;
sfrb SPL=0x3d;
sfrb SPH=0x3e;
sfrb SREG=0x3f;
#pragma used-

#asm
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
#endasm

typedef char *va_list;

#pragma used+

char getchar(void);
void putchar(char c);
void puts(char *str);
void putsf(char flash *str);

char *gets(char *str,unsigned int len);

void printf(char flash *fmtstr,...);
void sprintf(char *str, char flash *fmtstr,...);
void snprintf(char *str, unsigned int size, char flash *fmtstr,...);
void vprintf (char flash * fmtstr, va_list argptr);
void vsprintf (char *str, char flash * fmtstr, va_list argptr);
void vsnprintf (char *str, unsigned int size, char flash * fmtstr, va_list argptr);
signed char scanf(char flash *fmtstr,...);
signed char sscanf(char *str, char flash *fmtstr,...);

#pragma used-

#pragma library stdio.lib

typedef signed char     int8_t;
typedef signed int      int16_t;
typedef signed long int int32_t;

typedef unsigned char     uint8_t;
typedef unsigned int      uint16_t;
typedef unsigned long int uint32_t;

void rf_disable(void);
void rf_enable(void);
uint16_t pressed_keychain_status(void);
void pressed_keychain_status_clear(void);
void openkey_switch_routine(void);

void rf_capture_convert(uint8_t *out_arr);

#pragma used+

void delay_us(unsigned int n);
void delay_ms(unsigned int n);

#pragma used-

void hw_init(void);

uint8_t rf_capture_array[49 ]; 
uint8_t rf_capture_array_prev[49 ]; 
uint8_t rf_capture_cnt=0;
uint16_t rf_repeat_cnt=0;
uint8_t bit_8_capt_arr[6];

uint16_t t_half_1_bit=0;
uint16_t t_half_2_bit=0;

uint16_t delta_bit=3;

uint16_t keychain_status=0;
uint16_t keychain_short_status=0;

uint16_t openkey_sw_cnt=0;

uint16_t prnt_cnt=0;

uint16_t ovf_t1_cnt=0;

uint16_t get_timer1(void)
{
return TCNT1;    
}

void set_timer1(uint16_t tmr1_val)
{
TCNT1=tmr1_val;
}

void rf_disable(void)
{
GICR&=~(1<<6);
}

void rf_enable(void)
{
GICR|=(1<<6);
}

uint16_t pressed_keychain_status(void)
{
return keychain_status;
}

void pressed_keychain_status_clear(void)
{
keychain_status=0;
keychain_short_status=0;
}

unsigned char CRC8(const unsigned char *data, unsigned char len) 
{
unsigned char crc = 0x00;
while (len--) 
{
unsigned char extract = *data++;
unsigned char tempI;
for (tempI = 8; tempI; tempI--) 
{
unsigned char sum = (crc ^ extract) & 0x01;
crc >>= 1;
if (sum) 
{
crc ^= 0x8C;
}
extract >>= 1;
}
}
return crc;
}

void rf_capture_convert(uint8_t *out_arr)
{
uint8_t lng=0, lng_b=0;

for(lng=0, lng_b=0; lng<(3 *8); lng++)
{
if(lng%8==0)
{
if(lng!=0)
{
lng_b++;
}
out_arr[lng_b]=0x00;
}

if(rf_capture_array[lng+1])
{
out_arr[lng_b]|=((uint8_t)1<<(lng%8));

}
}
}

interrupt [9] void timer1_ovf_isr(void)
{
ovf_t1_cnt++;
}

interrupt [2] void ext_int0_isr(void)
{
uint32_t t_t_val=0;
uint16_t t1_val; 
uint16_t t_1_2_bit=0; 
uint16_t t_1_2_delta=0; 

uint8_t rf_capt8_arr[3 ]; 

t1_val=get_timer1();

ovf_t1_cnt=0;   

set_timer1(0x00); 

if(t1_val>12000 && t1_val<20000 && PIND.2  && !rf_capture_cnt) 

{
rf_capture_array[rf_capture_cnt]=0;
rf_capture_cnt=1; 

}
else if(rf_capture_cnt%2!=0 && !PIND.2  && rf_capture_cnt) 
{

t_half_1_bit=t1_val; 
rf_capture_cnt++;
}
else if(rf_capture_cnt%2==0 && PIND.2  && rf_capture_cnt) 
{

if(t1_val>50 && t1_val<150 ) 

{

rf_capture_cnt--;

set_timer1(t_half_1_bit); 

}
else
{
t_half_2_bit=t1_val;
t_1_2_bit=((uint32_t)t_half_1_bit+(uint32_t)t_half_2_bit)>>2; 
if(t_half_1_bit<t_half_2_bit)
{
t_1_2_delta=t_half_1_bit/delta_bit; 
if(t_1_2_bit>(t_half_1_bit-t_1_2_delta) && t_1_2_bit<(t_half_1_bit+t_1_2_delta))
{
rf_capture_array[rf_capture_cnt/2]=0; 
}
else
{
rf_capture_cnt=0; 
}
}
else if(t_half_1_bit>t_half_2_bit)
{
t_1_2_delta=t_half_2_bit/delta_bit;
if(t_1_2_bit>(t_half_2_bit-t_1_2_delta) && t_1_2_bit<(t_half_2_bit+t_1_2_delta))
{
rf_capture_array[rf_capture_cnt/2]=1; 
}
else
{
rf_capture_cnt=0; 
}
}
else 
{
rf_capture_cnt=0;
}

if(rf_capture_cnt) 
{
rf_capture_cnt++;
}
}
}

if(rf_capture_cnt)
{

if(rf_capture_array_prev[(rf_capture_cnt-1)>>1]==rf_capture_array[(rf_capture_cnt-1)>>1])

{

if(rf_repeat_cnt==10 ) 
{

rf_disable(); 

rf_enable();
}

else if(rf_repeat_cnt==3900)
{

keychain_status=keychain_short_status;
if(keychain_status&0b0000000000000100)
{
keychain_status|=0b0000000000001000;
}
else if(keychain_status&0b0000000000010000)
{
keychain_status|=0b0000000000100000;
}
else if(keychain_status&0b0000000001000000)
{
keychain_status|=0b0000000010000000;
}
else if(keychain_status&0b0000000100000000)
{
keychain_status|=0b0000001000000000;
}
else if(keychain_status&0b0000000000000001)
{
keychain_status|=0b0000000000000010;
}

}

if(rf_repeat_cnt<65500)
{
rf_repeat_cnt++;
}
}
else 
{
rf_repeat_cnt=0;
}
}
else 
{

if(keychain_short_status)
{
keychain_status|=(0b1000000000000000|keychain_short_status);
}
rf_repeat_cnt=0;
}

if((rf_capture_cnt>>1)>=(49 -1)) 
{
uint8_t cp=0;
uint16_t tmpr1t=0;
int16_t  tmpr2t=0;
rf_disable();
rf_capture_cnt=0;

PORTB.0=1;  

for(prnt_cnt=0; prnt_cnt<49 ; prnt_cnt++)
{
if((prnt_cnt/8)<6) 
{
if(prnt_cnt%8==0)bit_8_capt_arr[prnt_cnt/8]=0; 
bit_8_capt_arr[prnt_cnt/8]|=(rf_capture_array[prnt_cnt])<<(prnt_cnt%8);
}

}

if(CRC8(bit_8_capt_arr, 5)==bit_8_capt_arr[5])
{
printf("\n");
printf("ID:0x%04X ",((uint16_t)(bit_8_capt_arr[0]<<8)|bit_8_capt_arr[1]) );
printf("COMMAND:0x%02X ",(bit_8_capt_arr[2]) );
tmpr1t=(uint16_t)((bit_8_capt_arr[3]<<8)|bit_8_capt_arr[4]);
tmpr2t=((int16_t)tmpr1t-1000)/10;
if(bit_8_capt_arr[2]==0x05)
{
printf("tval=%04d ",tmpr1t);
printf("tdeg=%03d",tmpr2t);
}
if(bit_8_capt_arr[2]==0x0B)
{
printf("uval=%04d ",tmpr1t);
printf("uvolt=%03d",tmpr1t*11/10240);
}
}    

for(cp=0;cp<49 ;cp++)
{
rf_capture_array_prev[cp]=rf_capture_array[cp];
}
rf_enable();
}
}

