#include <mega8.h>
#include <stdio.h>
#include "rf_module.h"
#include "conf_board.h"
#include "init_hw.h"
#include "c_types.h"


//uint8_t capture_edge=CCP_CAPTURE_RE;
uint8_t rf_capture_array[KEY_BITS]; //int1
uint8_t rf_capture_array_prev[KEY_BITS]; //int1
uint8_t rf_capture_cnt=0;
uint16_t rf_repeat_cnt=0;
uint8_t bit_8_capt_arr[6];

// bit consist of 2 parts one part bigger in 3 times than another
uint16_t t_half_1_bit=0;
uint16_t t_half_2_bit=0;
// Coefficient which give maximum value of differense between signal pulses
// calculates as 100%/x% and must divide value for calculating percentage
uint16_t delta_bit=3;


uint16_t keychain_status=0;
uint16_t keychain_short_status=0;

uint16_t openkey_sw_cnt=0;

uint16_t prnt_cnt=0;

uint16_t ovf_t1_cnt=0;


uint16_t get_timer1(void)
{
    return TCNT1;    
}

void set_timer1(uint16_t tmr1_val)
{
    TCNT1=tmr1_val;
}

//stop code accepting
void rf_disable(void)
{
    EXT_INT0_DIS;
}

//start code accepting
void rf_enable(void)
{
    EXT_INT0_EN;
}

uint16_t pressed_keychain_status(void)
{
    return keychain_status;
}

void pressed_keychain_status_clear(void)
{
    keychain_status=0;
    keychain_short_status=0;
}

//CRC-8 - based on the CRC8 formulas by Dallas/Maxim
//code released under the therms of the GNU GPL 3.0 license
unsigned char CRC8(const unsigned char *data, unsigned char len) 
{
	unsigned char crc = 0x00;
	while (len--) 
	{
		unsigned char extract = *data++;
        unsigned char tempI;
		for (tempI = 8; tempI; tempI--) 
		{
			unsigned char sum = (crc ^ extract) & 0x01;
			crc >>= 1;
			if (sum) 
			{
				crc ^= 0x8C;
			}
			extract >>= 1;
		}
	}
	return crc;
}


//convert data from bool array to bytes array
//void rf_data_convert(int1 *in_arr, unsigned int8 *out_arr)  //WHY Don't work???
void rf_capture_convert(uint8_t *out_arr)
{
    uint8_t lng=0, lng_b=0;
    //convert 24 bytes only, and no start bit
    for(lng=0, lng_b=0; lng<(KEY_LENGTH*8); lng++)
    {
        if(lng%8==0)
        {
            if(lng!=0)
            {
                lng_b++;
            }
            out_arr[lng_b]=0x00;
        }
        
        if(rf_capture_array[lng+1])
        {
            out_arr[lng_b]|=((uint8_t)1<<(lng%8));
//        out_arr[lng_b]|=(1<<(lng%8));
        }
    }
}


// Timer 1 overflow interrupt service routine
interrupt [TIM1_OVF] void timer1_ovf_isr(void)
{
    ovf_t1_cnt++;
}


// External Interrupt 0 service routine
//interrupt [EXT_INT0] void ext_int0_isr(void)
//{
//    LED=!LED;
//}

//#INT_CCP1
//void  CCP1_isr(void)
interrupt [EXT_INT0] void ext_int0_isr(void)
{
    uint32_t t_t_val=0;
    uint16_t t1_val; //!=0;
    uint16_t t_1_2_bit=0; //middle value for comparing hi and lo pulses
    uint16_t t_1_2_delta=0; //middle value for calculating delta from short signal

    uint8_t rf_capt8_arr[KEY_LENGTH]; //array after transforming to 8bit

/*
    if(capture_edge==CCP_CAPTURE_RE) //if previous Edge was Rising (low level)
    {
        t1_val=get_timer1();
        set_timer1(0x00); //null(reset) timer1 //or timer0 ?????????
        capture_edge=CCP_CAPTURE_FE; //change edge
        setup_ccp1(CCP_CAPTURE_FE);
//        output_high(GRN_LED); //DEBUG
    }
    else if(capture_edge==CCP_CAPTURE_FE) //if previous Edge was Rising (hi level)
    {
        t1_val=get_timer1();
        set_timer1(0x00); //null(reset) timer1 //or timer0 ?????????
        capture_edge=CCP_CAPTURE_RE; //change edge
        setup_ccp1(CCP_CAPTURE_RE);
//        output_low(GRN_LED); //DEBUG
    }
*/
    t1_val=get_timer1();
//    t_t_val=(uint32_t)t1_val+(uint32_t)(0x0000FFFF*ovf_t1_cnt);
//    t_t_val=ovf_t1_cnt;   
    ovf_t1_cnt=0;   

    set_timer1(0x00); //null(reset) timer1 //or timer0 ?????????

//!!!!!!!!!!! now capture_edge is changed !!!!!!!!!!!
/*

//===============================ARTEFACT ERROR bits===============================
    if(t1_val>50 && t1_val<150 ) //catching by the low level artefacts
    {
//        t1_val=t_half_1_bit; //
        rf_capture_cnt--;
        capture_edge=CCP_CAPTURE_RE; //trick, to saving in t_half_1_bit
//        YEL_LED_TGL;
//        set_timer1(t_half_1_bit-100); //add prev val
//        set_timer1(200); //add prev val
    }
    if(t1_val>50 && t1_val<150 && capture_edge==CCP_CAPTURE_FE) //catching by the low level artefacts
    {
//        output_high(YEL_LED);
    }
    else if(t1_val>50 && t1_val<150 && capture_edge==CCP_CAPTURE_RE) //catching by the high level artefacts
    {
//        output_high(YEL_LED);
    }
*/
//===============================ARTEFACT ERROR bits===============================



    if(t1_val>12000 && t1_val<20000 && RF_IN_PIN && !rf_capture_cnt) //start stop //catching the low level pulse
//    if(t1_val>24000 && t1_val<40000 && RF_IN_PIN && !rf_capture_cnt) //start stop //catching the low level pulse
    {
        rf_capture_array[rf_capture_cnt]=0;
        rf_capture_cnt=1; //this mean that rf recording is started
//        YEL_LED_OFF;  //DEBUG
//          YEL_LED_TGL;  //DEBUG
    }
    else if(rf_capture_cnt%2!=0 && !RF_IN_PIN && rf_capture_cnt) //catching the high level pulse
    {
//        YEL_LED_ON;  //DEBUG
//===============================ARTEFACT ERROR bits===============================
        t_half_1_bit=t1_val; //save half bit time to compare with next half bit
        rf_capture_cnt++;
    }
    else if(rf_capture_cnt%2==0 && RF_IN_PIN && rf_capture_cnt) //catching the low level pulse
    {
//        YEL_LED_OFF; //DEBUG
        if(t1_val>50 && t1_val<150 ) //catching the low level artefacts
//        if(t1_val>100 && t1_val<300 ) //catching the low level artefacts
        {
    //        t1_val=t_half_1_bit; //
            rf_capture_cnt--;
    //        YEL_LED_TGL;  //DEBUG
            set_timer1(t_half_1_bit); //add prev val
    //        set_timer1(200); //add prev val
        }
        else
        {
            t_half_2_bit=t1_val;
            t_1_2_bit=((uint32_t)t_half_1_bit+(uint32_t)t_half_2_bit)>>2; //add 1/4 and 3/4 of sigal and divide by 4
            if(t_half_1_bit<t_half_2_bit)
            {
                t_1_2_delta=t_half_1_bit/delta_bit; //calculate delta deviations from one half bit time to another
                if(t_1_2_bit>(t_half_1_bit-t_1_2_delta) && t_1_2_bit<(t_half_1_bit+t_1_2_delta))
                {
                    rf_capture_array[rf_capture_cnt/2]=0; //save bit
                }
                else
                {
                    rf_capture_cnt=0; //reset counter for saving bits and start again
                }
            }
            else if(t_half_1_bit>t_half_2_bit)
            {
                t_1_2_delta=t_half_2_bit/delta_bit;
                if(t_1_2_bit>(t_half_2_bit-t_1_2_delta) && t_1_2_bit<(t_half_2_bit+t_1_2_delta))
                {
                    rf_capture_array[rf_capture_cnt/2]=1; //save bit
                }
                else
                {
                    rf_capture_cnt=0; //reset counter for saving bits and start again
                }
            }
            else // if(t_half_1_bit==t_half_2_bit) //may happends, who know
            {
                rf_capture_cnt=0;
            }

            if(rf_capture_cnt) //else if nulled - do not increment
            {
                rf_capture_cnt++;
            }
        }
    }


//==================catching repeats==================
    if(rf_capture_cnt)
    {
//        YEL_LED_TGL;  //DEBUG
        if(rf_capture_array_prev[(rf_capture_cnt-1)>>1]==rf_capture_array[(rf_capture_cnt-1)>>1])
//        if(rf_capture_array_prev[(rf_capture_cnt-1)/2]==rf_capture_array[(rf_capture_cnt-1)/2])
        {

            if(rf_repeat_cnt==SHORT_RF_PRESS) //sure pressing on keychain button
            {
//                EMPTY_NODE; //???????????????? but work
                rf_disable(); //for compare_key operations

/*
                rf_capture_convert(rf_capt8_arr);
                switch(compare_key_with_master(rf_capt8_arr))
                {
                    case master_button_A:
                    {
                        keychain_short_status|=UNLOCK_KEYCHAIN_SHORT; // SELECT THIS WHEN BUTTON A HAVE SIMILAR FUNCTION
//                        keychain_status|=keychain_short_status;
//                        keychain_status|=UNLOCK_KEYCHAIN_SHORT; // SELECT THIS WHEN BUTTON A HAVE SIMILAR FUNCTION
//                        keychain_status|=MASTER_BUTTON_A_SHORT;
                    }
                    break;
                    case master_button_B:
                    {
                        keychain_short_status|=MASTER_BUTTON_B_SHORT;
//                        keychain_status|=MASTER_BUTTON_B_SHORT;
                    }
                    break;
                    case master_button_C:
                    {
                        keychain_short_status|=MASTER_BUTTON_C_SHORT;
//                        keychain_status|=MASTER_BUTTON_C_SHORT;
                    }
                    break;
                    case master_button_D:
                    {
                        keychain_short_status|=MASTER_BUTTON_D_SHORT;
//                        keychain_status|=MASTER_BUTTON_D_SHORT;
                    }
                    break;
                    case master_button_other:
                    {
                        //NO ACTION
                    }
                    break;
                    default:
                    {
                        if(compare_key_with_ee(rf_capt8_arr))
                        {
                            keychain_short_status|=UNLOCK_KEYCHAIN_SHORT;
                            keychain_status|=keychain_short_status;
//                            keychain_status|=UNLOCK_KEYCHAIN_SHORT;
                        }
                    }
                    break;
                }
*/
//                rf_repeat_cnt++;
                rf_enable();
            }
//            else if(rf_repeat_cnt==LONG_RF_PRESS && !(save_keychains_status()&SAVE_K_EE_FL))
            else if(rf_repeat_cnt==LONG_RF_PRESS)
            {
//                EMPTY_NODE; //???????????????? but work
//                disable_interrupts(INT_CCP1);
//            keychain_status|=(ANY_KEYCHAIN_BUTTON_LEAVED|keychain_short_status);
                keychain_status=keychain_short_status;
                if(keychain_status&MASTER_BUTTON_A_SHORT)
                {
                    keychain_status|=MASTER_BUTTON_A_LONG;
                }
                else if(keychain_status&MASTER_BUTTON_B_SHORT)
                {
                    keychain_status|=MASTER_BUTTON_B_LONG;
                }
                else if(keychain_status&MASTER_BUTTON_C_SHORT)
                {
                    keychain_status|=MASTER_BUTTON_C_LONG;
                 }
                else if(keychain_status&MASTER_BUTTON_D_SHORT)
                {
                    keychain_status|=MASTER_BUTTON_D_LONG;
                }
                else if(keychain_status&UNLOCK_KEYCHAIN_SHORT)
                {
                    keychain_status|=UNLOCK_KEYCHAIN_LONG;
                }

//                rf_repeat_cnt++;
//                enable_interrupts(INT_CCP1);
            }

            if(rf_repeat_cnt<65500)
            {
                rf_repeat_cnt++;
            }
        }
        else //if not repeat
        {
            rf_repeat_cnt=0;
        }
    }
    else //if rf_capture_cnt==0
    {
//        if(keychain_status)
        if(keychain_short_status)
        {
            keychain_status|=(ANY_KEYCHAIN_BUTTON_LEAVED|keychain_short_status);
        }
        rf_repeat_cnt=0;
    }

//==================catching repeats==================

//if arrived all key package
    if((rf_capture_cnt>>1)>=(KEY_BITS-1)) //do not touch
    {
        uint8_t cp=0;
        uint16_t tmpr1t=0;
        int16_t  tmpr2t=0;
        rf_disable();
        rf_capture_cnt=0;
        
        YEL_LED_ON;  //DEBUG

//                printf("\n\rCode:\n\r");    
//        printf("\nOVF: %03d %d\n",t_t_val, t1_val); 
//        printf("\n");    
        for(prnt_cnt=0; prnt_cnt<KEY_BITS; prnt_cnt++)
        {
            if((prnt_cnt/8)<6) //convertion bit to byte
            {
                if(prnt_cnt%8==0)bit_8_capt_arr[prnt_cnt/8]=0; 
                bit_8_capt_arr[prnt_cnt/8]|=(rf_capture_array[prnt_cnt])<<(prnt_cnt%8);
            }

//            if(prnt_cnt%8==0)printf("  ");
//            printf("%d ",rf_capture_array[prnt_cnt]);
        }
        
//        printf("\n");    
//        for(prnt_cnt=0; prnt_cnt<6; prnt_cnt++)
//        {
//            printf("0x%02X ",bit_8_capt_arr[prnt_cnt]);
//        }
        
        if(CRC8(bit_8_capt_arr, 5)==bit_8_capt_arr[5])
        {
            printf("\n");
            printf("ID:0x%04X ",((uint16_t)(bit_8_capt_arr[0]<<8)|bit_8_capt_arr[1]) );
            printf("COMMAND:0x%02X ",(bit_8_capt_arr[2]) );
            tmpr1t=(uint16_t)((bit_8_capt_arr[3]<<8)|bit_8_capt_arr[4]);
            tmpr2t=((int16_t)tmpr1t-1000)/10;
            if(bit_8_capt_arr[2]==0x05)
            {
                printf("tval=%04d ",tmpr1t);
                printf("tdeg=%03d",tmpr2t);
            }
            if(bit_8_capt_arr[2]==0x0B)
            {
                printf("uval=%04d ",tmpr1t);
                printf("uvolt=%03d",tmpr1t*11/10240);
            }
        }    



        for(cp=0;cp<KEY_BITS;cp++)
        {
            rf_capture_array_prev[cp]=rf_capture_array[cp];
        }
        rf_enable();
    }
}

/*
void openkey_switch_routine(void)
{
    if(!END_SWITCH) //if KEY_SWITCH is pressed
    {
//        YEL_LED_TGL;
        if(openkey_sw_cnt>=KEY_SW_MAX)
        {
            //openkey_sw_on_cnt=KEY_SW_MAX;
        }
        else if(openkey_sw_cnt==KEY_SW_TRSHLD)
        {
            keychain_status=UNLOCK_KEYCHAIN_SHORT; //record bit
            openkey_sw_cnt++;
        }
        else if(openkey_sw_cnt==50)
        {
            keychain_status|=UNLOCK_KEYCHAIN_LONG; //record bit
            openkey_sw_cnt++;
        }
        else
        {
            openkey_sw_cnt++;
        }
    }
    else //if(!OPENKEY_SWITCH)
    {
        openkey_sw_cnt=0;
        keychain_status|=ANY_KEYCHAIN_BUTTON_LEAVED;

    }
}
*/