/*****************************************************
ATmega8
4,000000 MHz
*****************************************************/

#include <mega8.h>

#include <delay.h>
#include <stdlib.h>
#include <stdio.h>

#include <n3310lcd.c>
#include <pictures.c>

void main(void)
{
char i;

LcdInit();

while (1)
      	{

        LcdClear();
    	LcdImage(rad1Image); 
    	LcdUpdate();
	delay_ms(2000);
	LcdClear();
    	LcdImage(rad3Image); 
    	LcdUpdate();
	delay_ms(2000);
      
	LcdClear();
    	for (i=0; i<84; i++)
		{
		LcdLine ( 0, 47, i, 0, 1);
		LcdUpdate();
		delay_ms(10);
		}
    	for (i=0; i<48; i++)
		{
		LcdLine ( 0, 47, 83, i, 1);
		LcdUpdate();
		delay_ms(10);
		}

	LcdClear();
    	for (i=0; i<42; i++)
		{
		LcdCircle(42, 24, i, 1);
		LcdUpdate();
		delay_ms(15);
		}
	
	LcdClear();
	for (i=0; i<10; i++)
		{
		sprintf (lcd_buf, "�����=%u", i);
		LcdString(1,1);
		sprintf (lcd_buf, "�����=%u", i);
		LcdStringBold(1,2);
		sprintf (lcd_buf, "�����=%u/", i);
		LcdStringBig(1,4);
		sprintf (lcd_buf, "%u", i);
		LcdStringBold(9,4);
	        sprintf (lcd_buf, "�����=%u ", i+2);
	        LcdString(1,6);
		LcdUpdate();
		
		LcdBar(65, 27, 75, 45, 10*(i+1));
		
		delay_ms(300);
		}
	
	LcdMode(3);
	
	for (i=0; i<10; i++)
		{
		sprintf (lcd_buf, "�����=%u", i);
		LcdString(1,1);
		sprintf (lcd_buf, "�����=%u", i);
		LcdStringBold(1,2);
		sprintf (lcd_buf, "�����=%u/", i);
		LcdStringBig(1,4);
		sprintf (lcd_buf, "%u", i);
		LcdStringBold(9,4);
	        sprintf (lcd_buf, "�����=%u ", i+2);
	        LcdString(1,6);
		LcdUpdate();
		
		LcdBar(65, 27, 75, 45, 100-10*(i+1));
		
		delay_ms(300);
		}
	
	LcdMode(2);	
	};
}
